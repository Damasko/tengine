#!/usr/bin/python

import glob, os
import xml.etree.ElementTree as ET
import sys
import numpy as np

def main():

    for tmxFile in glob.glob("*.tmx"):
    
        tilemaps = []
        
        tree = ET.parse(tmxFile)
        root = tree.getroot()
        
        tileSizeX = int(root.attrib["tilewidth"])
        tileSizeY = int(root.attrib["tileheight"])
        mapWidth = int(root.attrib["width"])
        mapHeight = int(root.attrib["height"])

        for child in root:

            if child.tag == "layer":
                tiles = []
                for data in child:
                
                    for tile in data:
                          
                        tiles.append(tile.attrib["gid"])
                tilemaps.append(tiles)
        layer = 0
        for tiles in tilemaps:
            file = open("../" + tmxFile[:-4] + str(layer), 'w')
        
            tiles = np.asarray(tiles)
            shape = ( mapHeight, mapWidth )
            tiles.shape = shape
            tiles.reshape( shape ) 
            offset = (layer * 64)
            """
            for i in range(len(tiles)):
                file.write(str(i) + ":")
                for j in range(len(tiles[i]) - 1):
                    if tiles[i][j] != "0":
                        print("original " + str(tiles[i][j]))
                        print("offset   " + str(int(tiles[i][j]) - offset))
                        file.write(str(offset))
                    else:
                        file.write(str(int(tiles[i][j])) + ",")
                if (i != len(tiles) - 1):
                    if tiles[i][j] != "0":
                        file.write(str(int(tiles[i][j]) - offset) + ":s\n")
                    else:
                        file.write(str(int(tiles[i][j])) + ":s\n")
                    #file.write(str(int(tiles[i][-1]) - (layer * mapWidth * mapHeight)) + ":s\n")
                else:
                    if tiles[i][j] != "0":
                        file.write(str(int(tiles[i][j]) - offset) + ",")
                    else:
                        file.write(str(int(tiles[i][j])) + ",")
                    #file.write(str(int(tiles[i][-1]) - (layer * mapWidth * mapHeight)))
            file.write(":s\n")
            layer += 1
            """
            for i in range(len(tiles)):
                file.write(str(i) + ":")
                for j in range(len(tiles[i]) - 1):     
                    if layer != 0 and int(tiles[i][j]) != 0:

                        tiles[i][j] = int(tiles[i][j]) - offset
                        
                    file.write(str(tiles[i][j]) + ",")
                    
                if (i != len(tiles) - 1):
                    if layer != 0 and int(tiles[i][-1]) != 0:
                        tiles[i][-1] = int(tiles[i][-1]) - offset
                    file.write(str(tiles[i][-1]) + ":s\n")
                else:
                    if layer != 0 and int(tiles[i][-1]) != 0:
                        tiles[i][-1] = int(tiles[i][-1]) - offset
                    file.write(str(tiles[i][-1]))
                                
            file.write(":s\n")    
            layer += 1
                
                
                
                
                
                
                  
main()
