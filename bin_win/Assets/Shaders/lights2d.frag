#ifdef GL_ES
precision highp float;
#endif

varying vec4 color;
varying vec2 texCoord;

uniform sampler2D tex;

uniform vec2 screen;

uniform vec2 cameraPos;
uniform vec2 lightPos1;
uniform vec2 lightPos2;
uniform vec2 lightPos3;
uniform vec2 lightPos4;
uniform vec2 lightPos5;

vec4 toLinear (vec4 color){
    return pow(color,vec4(2.2));
}

vec4 toGamma (vec4 color){
    return pow(color,vec4(1.0/2.2));
}


vec3 reinhardRoboTonemap(vec3 c,float a){
    return c/pow(1.+pow(c,vec3(a) ),vec3(1./a) );
}
vec3 roboTonemap(vec3 c){
    return c/sqrt(c*c+1.);
}

vec3 jodieRoboTonemap(vec3 c){
    float l = dot(c, vec3(0.2126, 0.7152, 0.0722));
    vec3 tc=c/sqrt(c*c+1.);
    return mix(c/sqrt(l*l+1.),tc,tc);
}


vec4 dithering( vec2 vScreenPos ){
    vec3 vDither = vec3( dot( vec2( 171.0, 231.0 ), vScreenPos.xy ) );
    vDither.rgb = fract( vDither.rgb / vec3( 103.0, 71.0, 97.0 ) );
    return vec4(vDither.rgb / 255.0,1.0);
}


vec4 calculateLight(vec2 foo,vec2 lightPos,vec4 color,float intensity){

    intensity = min(100.0, intensity);
    float dist = distance(foo,lightPos.xy)/intensity;
    float lightness = 1.0/(dist); //inverse quare law
    return color*color*lightness;
    //float dist = distance(foo,lightPos.xy);
    //return color * max(0.0, min(1.0, (0.2 * intensity * intensity) / sqrt(dist)));

}

vec2 lengthDir(float dist,float dir){

    return vec2(cos(dir)*dist,sin(dir)*dist);

}


void main() {
    /*
    vec2 movingLight=iResolution.xy*vec2(0.5,0.5)+lengthDir(80.0,iGlobalTime/2.0);
    vec4 color =
        calculateLight(fragCoord.xy,iMouse.xy,vec4(1.0,0.5,0.1,1.0),30.0)+
        calculateLight(fragCoord.xy,iResolution.xy*vec2(0.5,0.5),vec4(0.1,0.5,1.0,1.0),50.0)+
        calculateLight(fragCoord.xy,movingLight,vec4(1.0,0.1,0.05,1.0),10.0);
    */

    vec2 lP1Fix = (lightPos1 - cameraPos) / screen.xy;
    vec2 lP2Fix = (lightPos2 - cameraPos) / screen.xy;
    vec2 lP3Fix = (lightPos3 - cameraPos) / screen.xy;
    vec2 lP4Fix = (lightPos4 - cameraPos) / screen.xy;
    vec2 lP5Fix = (lightPos5 - cameraPos) / screen.xy;
    vec4 color = calculateLight(gl_FragCoord.xy, screen.xy * lP1Fix, vec4(0.1,0.5,1.0,1.0), 150.0)
             + calculateLight(gl_FragCoord.xy, screen.xy * lP2Fix, vec4(0.9,0.3,0.2,1.0), 70.0)
             + calculateLight(gl_FragCoord.xy, screen.xy * lP3Fix, vec4(0.0,1.0,0.6, 1.0), 40.0)
             + calculateLight(gl_FragCoord.xy, screen.xy * lP4Fix, vec4(0.6,0.2,0.9,1.0), 80.0)
             + calculateLight(gl_FragCoord.xy, screen.xy * lP5Fix, vec4(0.8,0.1,0.1,1.0), 90.0);

    //handle overexposed colors (just to make things pretty)
    //color.rgb=jodieRoboTonemap(color.rgb);
    //color.rgb=roboTonemap(color.rgb);

    //color=toGamma(color);//convert linear colors to srgb
    color+=dithering(gl_FragCoord.xy); //prevent banding (just to make things pretty)

    gl_FragColor = color;

 }
