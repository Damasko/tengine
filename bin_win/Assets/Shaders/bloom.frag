#ifdef GL_ES
precision highp float;
#endif

varying vec4 color;
varying vec2 texCoord;

uniform sampler2D tex;
uniform int steps;


void main(void) {

    vec4 sum = vec4(0.0);
    vec4 sum2 = vec4(0.0);
    int j;
    int i;

    for( i= -steps ;i < steps; ++i) {
        for (j = -steps; j < steps; ++j) {

            sum += texture2D(tex, texCoord + vec2(j, i) * 0.003) * 0.03;

        }
    }

    gl_FragColor = sum + texture2D(tex, texCoord);

}


