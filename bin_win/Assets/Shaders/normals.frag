#ifdef GL_ES
precision highp float;
#endif

varying vec4 color;
varying vec2 texCoord;

uniform sampler2D tex;

uniform vec2 screen;


void main() {

    vec2 uv = texCoord;

    float x=1.;
    float y=1.;

    float L =abs(texture2D(tex, uv + vec2(x, 0.0)/ screen.xy).r);
    float R =abs(texture2D(tex, uv + vec2(-x, 0.0)/ screen.xy).r);
    float U =abs(texture2D(tex, uv + vec2(0.0, y)/ screen.xy).r);
    float D =abs(texture2D(tex, uv + vec2(0.0, -y)/ screen.xy).r);
    float X = (R-L)*0.5;
    float Y = -(D-U)*0.5;

    float strength =0.0005;
    vec4 N = vec4(normalize(vec3(X, Y, strength)), 1.0);

    vec4 col = vec4(N.xyz * 0.5 + 0.5, 1.0);

    gl_FragColor = col;
    //gl_FragColor = texture2D(tex, uv) * vec4(0.1, 0.8, 0.8, 1.0);

}
