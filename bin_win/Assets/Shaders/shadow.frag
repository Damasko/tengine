#ifdef GL_ES
precision highp float;
#endif

varying vec4 color;
varying vec2 texCoord;

uniform sampler2D tex;
uniform sampler2D lightTex;

void main() {

    /*
    vec2 lightPos = vec2(0.5, 0.5);
    vec2 lightDir = gl_FragCoord.xy - lightPos;
    normalize(lightDir);
    vec4 res;
    vec2 uv = texCoord;
    for (int i = 0; i < 6; ++i) {
        res += texture2D(tex, uv);
        uv += float(i) * 0.00001 * lightDir * res.a;
    }
    gl_FragColor = res;
    */
    vec4 color = texture2D(tex, texCoord);
    vec4 light = texture2D(lightTex, texCoord);
    gl_FragColor = light;

}
