TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += /usr/include/SDL2 \

LIBS += -L /usr/lib -lSDL2 \
                    -lSDL2_mixer \
                    -lSDL2_ttf \
                    -lSDL2_gpu \
                    -lSDL2_net \
                    -lBox2D \

HEADERS += \
    Core/Engine/engine.hpp \
    Core/Math/vector2.hpp \
    Core/Scene/scene.hpp \
    Core/Systems/InputSystem/inputsystem.hpp \
    Core/Systems/ISystem/system.hpp \
    Core/Systems/RenderingSystem/renderingsystem.hpp \
    Core/Material/shader.hpp \
    Core/Config/configuration.hpp \
    Core/Message/message.hpp \
    Core/Tools/stringtools.hpp \
    Core/GameObject/gameobjectfactory.hpp \
    Core/GameObject/gameobject.hpp \
    Core/Component/Base/component.hpp \
    Core/Component/GraphicComponent/graphiccomponent.hpp \
    Core/Component/GraphicComponent/sprite.hpp \
    Core/Scene/scenefactory.hpp \
    Core/Timer/timer.hpp \
    Core/Component/Transform/transform.hpp \
    Core/Math/vector3.hpp \
    Core/Texture/texture.hpp \
    Core/Component/Camera/camera2D.hpp \
    Core/Multimedia/multimedia.hpp \
    Core/Multimedia/images.hpp \
    Core/Multimedia/sounds.hpp \
    Core/Math/mathtools.hpp \
    Core/Component/GraphicComponent/tilemap.hpp \
    Core/Texture/tileset.hpp \
    Core/Texture/regions.hpp \
    Core/Component/InputComponent/inputcomponent.hpp \
    Core/Component/InputComponent/followmouse.hpp \
    Core/Tools/gametools.hpp \
    Core/Systems/PhysicsSystem/physicssystem.hpp \
    Core/Component/UIComponent/uicomponent.hpp \
    Core/Component/GraphicComponent/shape.hpp

SOURCES += main.cpp \
    Core/Engine/engine.cpp \
    Core/Math/vector2.cpp \
    Core/Scene/scene.cpp \
    Core/Systems/InputSystem/inputsystem.cpp \
    Core/Systems/LogicSystem/c.cc \
    Core/Systems/PhysicsSystem/d.cc \
    Core/Systems/SoundSystem/f.cc \
    Core/Systems/RenderingSystem/renderingsystem.cpp \
    Core/Material/shader.cpp \
    Core/Config/configuration.cpp \
    Core/Message/message.cpp \
    Core/Component/PhysicComponent/pc.cc \
    Core/Component/SoundComponent/sc.cc \
    Core/GameObject/gameobjectfactory.cpp \
    Core/GameObject/gameobject.cpp \
    Core/Component/GraphicComponent/graphiccomponent.cpp \
    Core/Component/GraphicComponent/sprite.cpp \
    Core/Scene/scenefactory.cpp \
    Core/Timer/timer.cpp \
    Core/Component/Transform/transform.cpp \
    Core/Component/Base/component.cpp \
    Core/Math/vector3.cpp \
    Core/Texture/texture.cpp \
    Core/Component/Camera/camera2D.cpp \
    Core/Multimedia/multimedia.cpp \
    Core/Multimedia/images.cpp \
    Core/Multimedia/sounds.cpp \
    Core/Component/GraphicComponent/tilemap.cpp \
    Core/Texture/tileset.cpp \
    Core/Texture/regions.cpp \
    Core/Component/InputComponent/inputcomponent.cpp \
    Core/Component/InputComponent/followmouse.cpp \
    Core/Systems/PhysicsSystem/physicssystem.cpp \
    Core/Component/UIComponent/uicomponent.cpp \
    Core/Component/GraphicComponent/shape.cpp

DISTFILES += \
    ../bin/Assets/Shaders/bloom.frag \
    ../bin/Assets/Shaders/normals.frag \
    ../bin/Assets/Shaders/texture.frag \
    ../bin/Assets/Shaders/water.frag \
    ../bin/Assets/Shaders/normals.vert \
    ../bin/Assets/Shaders/water.vert \
    ../bin/Assets/Shaders/bloom.vert \
    ../bin/Assets/Shaders/texture.vert \
    ../bin/Assets/Shaders/lights2d.frag \
    ../bin/Assets/Shaders/lights2d.vert \
    ../bin/Assets/Shaders/deferred2d.frag \
    ../bin/Assets/Shaders/deferred2d.vert \
    ../bin/Assets/Shaders/emission.frag \
    ../bin/Assets/Shaders/emission.vert \
    ../bin/Assets/Shaders/depth.frag \
    ../bin/Assets/Shaders/depth.vert \
    ../bin/Assets/Shaders/shadow.frag \
    ../bin/Assets/Shaders/shadow.vert

