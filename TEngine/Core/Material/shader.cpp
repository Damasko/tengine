#include "shader.hpp"


Shader::Shader() {


}

void Shader::loadFromFile(GPU_ShaderEnum type, std::string path) {

    program = GPU_LoadShader(type, path.c_str());

}

bool Shader::loadBlock(std::string filename, std::string path) {

    if (path.empty())
        path = Configuration::get().assetFolder + Configuration::get().shadersFolder;
    bool success = true;

    std::string fullPathVert = (path + filename + ".vert");
    std::string fullPathFrag = (path + filename + ".frag");

    vertex = GPU_LoadShader(GPU_VERTEX_SHADER, fullPathVert.c_str());

    if (!vertex) {
        GPU_LogError("Failed to load vertex shader: %s\n", GPU_GetShaderMessage());
        success = false;
    }

    fragment = GPU_LoadShader(GPU_FRAGMENT_SHADER, fullPathFrag.c_str());

    if(!fragment) {
        GPU_LogError("Failed to load fragment shader: %s\n", GPU_GetShaderMessage());
        success = false;

    }

    program = GPU_LinkShaders(vertex, fragment);

    if(!program) {
        GPU_ShaderBlock b = {-1, -1, -1, -1};
        GPU_LogError("Failed to link shader program: %s\n", GPU_GetShaderMessage());
        success = false;
        block = b;
    }

    if (!success) {
        SDL_Log("Cannot compile shader %s", filename.c_str());
    }

    block = GPU_LoadShaderBlock(program, "gpu_Vertex", "gpu_TexCoord", NULL, "gpu_ModelViewProjectionMatrix");
    GPU_ActivateShaderProgram(program, &block);

    return success;

}

void Shader::activate() {

    GPU_ActivateShaderProgram(program, &block);

}

void Shader::disable() {

    GPU_DeactivateShaderProgram();

}

void Shader::setUniform(std::string uniformName, int value) {

    GPU_ActivateShaderProgram(program, &block);
    int loc = GPU_GetUniformLocation(program, uniformName.c_str());
    if (loc == -1) {
        SDL_Log("Error: Invalid uniform location");
        return;
    }

    GPU_SetUniformi(loc, value);
    GPU_DeactivateShaderProgram();

}

void Shader::cleanUp() {

    disable();
    GPU_FreeShader(vertex);
    GPU_FreeShader(fragment);
    GPU_FreeShaderProgram(program);

}

// TODO!

bool Shader::isActive() {

    //glsha
    return false;

}
