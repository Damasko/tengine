#ifndef SHADER_HPP
#define SHADER_HPP

#include "SDL_gpu.h"
#include <string>

#include "Core/Config/configuration.hpp"

class Shader {

    public:

        Uint32              vertex;
        Uint32              fragment;
        Uint32              program;
        GPU_ShaderBlock     block;

    public:

        Shader();
        void loadFromFile(GPU_ShaderEnum type, std::string path);
        bool loadBlock(std::string filename, std::string path="");

        void setUniform(std::string uniformName, int value);

        void activate();
        void disable();
        void cleanUp();

        bool isActive();

};

#endif
