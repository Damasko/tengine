#ifndef STRINGTOOLS_HPP
#define STRINGTOOLS_HPP

#include <string>
#include <sstream>
#include <vector>


class StringTools {

    private:

        StringTools();

    public:

        template <typename T> static inline std::string to_string(T value) {
            std::ostringstream os ;
            os << value ;
            return os.str() ;
        }

        static inline std::vector<std::string> splitString(const char *str, char token='|') {

            std::vector<std::string> result;

            do  {
                const char *begin = str;

                while(*str != token && *str)
                    str++;

                result.push_back(std::string(begin, str));
            } while (0 != *str++);

            return result;

        }

        static inline int to_integer(const std::string & value) {

            int res = 0;
            std::istringstream stream(value);
            stream >> res;
            return res;

        }

        static inline long to_long(const std::string & value) {

            long res = 0;
            std::istringstream stream(value);
            stream >> res;
            return res;

        }


};

#endif // STRINGTOOLS_HPP
