#ifndef GAMETOOLS_HPP
#define GAMETOOLS_HPP

#ifndef GAMETOOLS_H
#define GAMETOOLS_H

#include "SDL_gpu.h"
#include "Core/Math/vector2.hpp"

/* Esto to_string,  no deberia estar aqui, pero parece que ndk no reconoce esa funcion de c++11, asi que es la manera casera */

#include <string>
#include <sstream>


namespace GameTools {

    static const double     PI      = 3.141592;
    static const double     DEGREES = (180.0 / PI);
    static const double     RADIANS = (PI / 180.0);

    inline bool rectColliding(const GPU_Rect *a, const GPU_Rect *b) {

        if( (a->y + a->h) < b->y)
            return false;

        if( a->y > (b->y + b->h))
            return false;

        if( (a->x + a->w) < b->x)
            return false;

        if( a->x > (b->x + b->w))
            return false;

        return true;

    }

    /*
    inline bool circleEdgeColliding(Vector2 edge, Vector2 circlePosition, float radius) {
        // TODO
        return true;

    }
    */

    inline bool rectCircleColliding(const GPU_Rect *rect, Vector2 circleOrigin, float radius) {

        Vector2 circleDistance;
        circleDistance.x = abs(circleOrigin.x - (rect->x + rect->w / 2));
        circleDistance.y = abs(circleOrigin.y - (rect->y + rect->h / 2));

        if (circleDistance.x > (rect->w / 2 + radius)) { return false; }
        if (circleDistance.y > (rect->h / 2 + radius)) { return false; }

        if (circleDistance.x <= (rect->w / 2)) { return true; }
        if (circleDistance.y <= (rect->h / 2)) { return true; }

        float cornerDistance_sq = pow((circleDistance.x - rect->w / 2 ), 2 ) + pow((circleDistance.y - rect->h / 2), 2);

        return (cornerDistance_sq <= (radius * radius));

    }

    inline bool rectPointColliding(const GPU_Rect *rect, const Vector2 point) {

        if (point.x < rect->x or point.x > rect->x + rect->w) {
            return false;
        }
        if (point.y < rect->y or point.y > rect->y + rect->h) {
            return false;
        }

        return true;

    }

    inline bool circlesColliding(Vector2 center1, Vector2 center2, float radius1, float radius2) {

        float length = radius1 + radius2;
        //squared
        length *= length;

        Vector2 circlesVector = center2 - center1;

        if (circlesVector.squaredDistance() <= length) {
            return true;
        }

        return false;
    }

    template<class T>
    inline void clamp( T& v, const T& lo, const T& hi ) {
        if (v < lo) v = lo;
        else if (v > hi) v = hi;
    }


    inline float getPercent(float parent, float percent) {

        return (parent * percent) / 100.0f;

    }

    inline float getRandFloatNorm() {

        return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

    }

    inline float lerp(float a, float b, float f) {
        return (a * (1.0 - f)) + (b * f);
    }

}


#endif // GAMETOOLS_HPP



#endif // GAMETOOLS_HPP
