#include "component.hpp"
#include "Core/GameObject/gameobject.hpp"

Component::Component(GameObject *owner) : owner(owner), systems(SYS_NONE) {

    name = "Empyy";
    ID = 0;

}

Component::Component(std::string name, unsigned int ID) {

    this->name  = name;
    this->ID    = ID;

}

GameObject* Component::getOwner() const {

    return owner;

}

unsigned int Component::getSystems() {

    return systems;

}

bool Component::hasToRegisterInSystem(Component *component, const unsigned int system) {

    return (component->getSystems() & system) == system;

}
