#ifndef COMPONENT_HPP
#define COMPONENT_HPP

#include <string>

class GameObject;

const unsigned int SYS_NONE        = 0;
const unsigned int SYS_RENDERING   = 1;
const unsigned int SYS_PHYSICS     = 2;
const unsigned int SYS_INPUT       = 4;
const unsigned int SYS_MOVEMENT    = 8;
const unsigned int SYS_SOUND       = 16;


class Component {

    protected:

        std::string     name;
        unsigned int    ID;
        //int             layer;
        GameObject      *owner;

        unsigned int    systems;

    public:

        Component(GameObject *owner);
        Component(std::string name, unsigned int ID);
        virtual ~Component() {}
        virtual void init()          = 0;
        virtual void update()        = 0;
        virtual void fixedUpdate()   = 0;
        virtual void onExit()        = 0;
        virtual void reset()         = 0;
        virtual void cleanUp()       = 0;

        GameObject* getOwner() const;

        unsigned int getSystems();
        static bool hasToRegisterInSystem(Component *component, const unsigned int system);

};

#endif // COMPONENT_HPP
