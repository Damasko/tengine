#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#include "Core/Component/Base/component.hpp"
#include "Core/Math/vector3.hpp"
#include "Core/Math/vector2.hpp"
#include "Core/Math/mathtools.hpp"

class Transform : public Component {

    private:

        Vector3     position;
        Vector3     scale;
        Vector3     rotation;
        float       rotation2D;

    public:

        Transform(GameObject *owner);
        ~Transform();
        virtual void    init();
        virtual void    update();
        virtual void    fixedUpdate();
        virtual void    onExit();
        virtual void    reset();
        virtual void    cleanUp();

        void            setPosition(Vector3 npos);
        void            setPosition(Vector2 npos);

        void            setRotation(Vector2 rotation);
        void            setRotation(Vector3 rotation);
        void            setRotation(float rot);

        void            setScale(Vector2 scale);
        void            setScale(Vector3 scale);

        Vector3         getPosition();
        Vector2         getPosition2D();
        Vector3         getScale();
        Vector3         getRotation();
        double          getRotation2D();

};

#endif // TRANSFORM_HPP
