#include "transform.hpp"


Transform::Transform(GameObject *owner) : Component(owner) {


}

Transform::~Transform() {


}

void Transform::init() {

    position = Vector3(0.0f, 0.0f, 0.0f);
    scale = Vector3(1.0f, 1.0f, 1.0f);
    rotation = Vector3(0.0f, 0.0f, 0.0f);
    rotation2D = 0.0f;

}

void Transform::update() {

}

void Transform::fixedUpdate() {


}

void Transform::onExit() {

}

void Transform::reset() {

}

void Transform::cleanUp() {

}

void Transform::setPosition(Vector3 npos) {

    position = npos;

}

void Transform::setPosition(Vector2 npos) {

    position.x = npos.x;
    position.y = npos.y;
    position.z = 0;

}

void Transform::setRotation(Vector2 rotation) {

    this->rotation.x = rotation.x;
    this->rotation.y = rotation.y;

}

void Transform::setRotation(Vector3 rotation) {

    this->rotation = rotation;

}

void Transform::setRotation(float rot) {

    rotation2D = rot;

}

void Transform::setScale(Vector2 scale) {

    this->scale.x = scale.x;
    this->scale.y = scale.y;

}

void Transform::setScale(Vector3 scale) {

    this->scale = scale;

}


Vector3 Transform::getPosition() {

    return position;

}

Vector2 Transform::getPosition2D() {

    return Vector2(position.x, position.y);

}

Vector3 Transform::getScale() {

    return scale;

}

Vector3 Transform::getRotation() {

    return rotation;

}

double Transform::getRotation2D() {

    //return atan2(rotation.y, rotation.x) * (180 / MathTools::PI);
    return rotation2D;

}
