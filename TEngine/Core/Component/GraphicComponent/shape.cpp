#include "shape.hpp"

Shape::Shape(GameObject *owner) : GraphicComponent(owner), type(RECTANGLE), filled(true) {

}

ShapeType Shape::getType() {

    return type;

}

bool Shape::isFilled() {

    return filled;

}

std::vector<float> *Shape::getValues() {

    return &values;

}
