#include "tilemap.hpp"
#include "Core/GameObject/gameobject.hpp"
#include "Core/Systems/RenderingSystem/renderingsystem.hpp"

Tilemap::Tilemap(GameObject *owner) : GraphicComponent(owner){

    systems = SYS_RENDERING /* & SYS_PHYSICS */;
    area.x = 0;
    area.y = 0;
    name = "Tilemap";

}

Tilemap::~Tilemap() {

    tiles.clear();

}

 void Tilemap::init() {

    area.x = owner->getTransform()->getPosition2D().x;
    area.y = owner->getTransform()->getPosition2D().y;

 }

int Tilemap::loadFromFile(std::string mapName, std::string path) {

    if (path.empty()) {
        path = Configuration::get().assetFolder + Configuration::get().mapsFolder;
    }

    Message mapTiles;
    if (mapTiles.loadFromFile(path + mapName) < 0)
        return -1;

    std::vector<std::pair<std::string, std::string> > rows = mapTiles.getAllStrings();

    // Cambiar para elegir W y H de destiny rect;

    Vector2 mapSize = Vector2(StringTools::splitString(rows[0].second.c_str(), ',').size(), rows.size());
    setMapSize(mapSize);
    setBaseTileSize(Vector2(128, 128));

    area.w = mapW * dstW;
    area.h = mapH * dstH;

    std::sort(rows.begin(), rows.end());

    for (unsigned int i = 0; i < rows.size(); ++i) {

        std::vector<std::string> tilesRow = StringTools::splitString(rows[i].second.c_str(), ',');

        for (unsigned int j = 0; j < tilesRow.size(); ++j) {
            Tile tile;
            tile.id = StringTools::to_integer(tilesRow[j]);
            tile.src.w = dstW;
            tile.src.h = dstH;
            if (tilesRow[j] == "0") {
                tile.properties = 0;
            }
            else {
                setTile(&tile);
            }
            // Quiza deberia poner el centro en vez de la esquina superior, en caso de error a la hora de dibujar y colision puede
            // ser esta la causa
            tile.position = Vector2(j * dstW, i * dstH);

            tiles.emplace_back(tile);

        }

    }

    return 0;
}

int Tilemap::loadTileset(GPU_Image *texture, std::string name) {

    if (tileset.load(name) < 0) {

        return -1;

    }

    tileset.setImage(texture);

    return 0;

}

int Tilemap::load(std::string mapName, std::string tilesetName, GPU_Image *tilesetImg) {

    if (loadFromFile(mapName) < 0) {
        SDL_Log("Tilemap: Error while loading tilemap. Map name:   %s", mapName.c_str());
        return -1;
    }
    if (loadTileset(tilesetImg, tilesetName) < 0) {
        SDL_Log("Tilemap: Error while loading tileset. Tileset name:   %s", tilesetName.c_str());
        return -2;
    }

    return 0;

}

std::vector<Tile> *Tilemap::getTiles() {

    return &tiles;

}

std::vector<Tile*> Tilemap::getVisibleTiles(Camera2D *camera) {

    std::vector<Tile*> res;

    GPU_Rect *cameraBounds = camera->getRect();
    Vector3 scale = owner->getTransform()->getScale();
    int originX = (cameraBounds->x - area.x) / dstW;
    int originY = (cameraBounds->y - area.y) / dstH;
    if (originX < 0) originX = 0;
    if (originY < 0) originY = 0;
    if (originX > area.x + area.w) return res;
    if (originY > area.y + area.h) return res;
    int tilesX = cameraBounds->w / (dstW * scale.x);
    int tilesY = cameraBounds->h / (dstH * scale.y);

    // OJO: Dividir entre 0 con floats NO lanza ningun error como si lo hace con integers!
    // https://en.wikipedia.org/wiki/Division_by_zero#In_computer_arithmetic

    for (unsigned int i = originY; i < originY + tilesY + 2; ++i) {
        for (unsigned int j = originX; j < originX + tilesX + 2; ++j) {
            Tile *tile = getTile(j, i);
            if (tile) {
                res.emplace_back(tile);
            }
        }

    }

    return res;

}


void Tilemap::setMapSize(Vector2 size) {

    mapW = size.x;
    mapH = size.y;

}

void Tilemap::setBaseTileSize(Vector2 size) {

    dstW = size.x;
    dstH = size.y;

}

void Tilemap::setPosition(Vector2 newPos) {




}


Tile *Tilemap::getTile(int x, int y) {

    if (x < 0 or y < 0 or x >= mapW or y >= mapH) {
        return NULL;
    }
    return &tiles[(y * mapW) + x];

}

Tile *Tilemap::getTileFromPosition(Vector2 position) {

    Vector2 fixed;
    fixed.x = (position.x - area.x) / dstW;
    fixed.y = (position.y - area.y) / dstH;

    return getTile((int) round(fixed.x), (int) round(fixed.y));

}

std::vector<Tile *> Tilemap::getTilesArea(GPU_Rect *target) {

    std::vector<Tile*> result;

    int x = (target->x - area.x) / dstW - 1;
    int y = (target->y - area.y) / dstH - 1;

    int howManyX = 1 + target->w / dstW;
    int howManyY = 2 + target->h / dstH;

    for (int i = y - 1; i <= y + howManyY; ++i) {
        for (int j = x - 1; j <= x + howManyX; ++j) {

            result.emplace_back(getTile(j, i));

        }
    }

    return result;

}

Tileset *Tilemap::getTileset() {

    return &tileset;

}

GPU_Rect *Tilemap::getDstRect() {

    return &area;

}


void Tilemap::setTile(Tile *target) {

    Region *data = tileset.getRegion((StringTools::to_string(target->id)));

    target->src = data->src;
    target->properties = data->properties;

}

int Tilemap::getMapW() {

    return mapW;

}

int Tilemap::getMapH() {

    return mapH;

}

int Tilemap::getTileW() {

    return dstW;

}

int Tilemap::getTileH() {

    return dstH;

}

void Tilemap::cleanUp() {



}

void Tilemap::accept(RenderingSystem *system) {

    system->render(this);

}


/*
bool pairCompare(const std::pair<std::string, std::string> > &a, const std::pair<std::string, std::string> > &b) {

    return StringTools::to_integer(a.first) < StringTools::to_integer(b.first);

}
*/

/*

Block *BlockMap::getBlock(int x, int y) {

    if (x < 0 or y < 0 or x >= MAP_DEFAULT_W or y >= MAP_DEFAULT_H) {
        return NULL;
    }
    return &tiles[(y * MAP_DEFAULT_W) + x];

}

Block *BlockMap::getBlockFromPos(float x, float y) {

    // no testeado!
    x -= area.x;
    y -= area.y;

    x /= BLOCK_DST_W;
    y /= BLOCK_DST_H;

    return getBlock((int) round(x), (int) round(y));

}

Block *BlockMap::getBlockFromPos(Vector2 pos) {

    // no testeado!
    int x = (int) pos.x;
    int y = (int) pos.y;
    x -= area.x;
    y -= area.y;

    x /= BLOCK_DST_W;
    y /= BLOCK_DST_H;

    return getBlock(x, y);

}

std::vector<Block *> BlockMap::getCollisionBlocks(float px, float py, int areaRangeX, int areaRangeY) {

    std::vector<Block*> collideBlocks;

    int x = (px - area.x) / BLOCK_DST_W;
    int y = (py - area.y) / BLOCK_DST_H;

    for (int i = -areaRangeY; i <= areaRangeY; ++i) {
        for (int j = -areaRangeX; j <= areaRangeX; ++j) {
            collideBlocks.push_back(getBlock(x + j, y + i));

        }
    }

    return collideBlocks;

}

*/
