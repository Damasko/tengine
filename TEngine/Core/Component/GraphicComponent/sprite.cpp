#include "sprite.hpp"
#include "Core/Systems/RenderingSystem/renderingsystem.hpp"

Sprite::Sprite(GameObject *owner) : GraphicComponent(owner), image(NULL), flipped(false), hidden(false) {

    setSrc(0, 0, 1, 1);
    setDstW(32);
    setDstH(32);
    setCenter(owner->getTransform()->getPosition2D());
    name = "Sprite";

}

Sprite::Sprite(GameObject *owner, float w, float h) : GraphicComponent(owner) {

    src.x = src.y = 0;
    src.w = dst.w = w;
    src.h =  dst.h = h;
    setCenter(owner->getTransform()->getPosition2D());

}

Sprite::~Sprite() {



}

bool Sprite::collide(Sprite *a, Sprite *b) {

    if( a->getBottom() <= b->getTop())
        return false;

    if( a->getTop() >= b->getBottom())
        return false;

    if( a->getRight() <= b->getLeft())
        return false;

    if( a->getLeft() >= b->getRight())
        return false;

    return true;

}


float Sprite::getLeft() {

    return dst.x;

}

float Sprite::getRight() {

    return dst.x + dst.w;

}

float Sprite::getTop() {

    return dst.y;

}

float Sprite::getBottom() {

    return dst.y + dst.h;

}


float Sprite::getCenterX() {

    return dst.x + (dst.w / 2);

}

float Sprite::getCenterY() {

    return dst.y + (dst.h / 2);

}

Vector2 Sprite::getCenter() {

    return Vector2(dst.x + (dst.w / 2), dst.y + (dst.h / 2));

}

GPU_Rect *Sprite::getDstRect() {

    return &dst;

}

GPU_Rect *Sprite::getSrcRect() {

    return &src;

}

int Sprite::getImageW() {
    /*
    int w;
    SDL_QueryTexture(image, NULL, NULL, &w, NULL);
    */
    if (image)
        return image->w;
    return -1;

}

int Sprite::getImageH() {
    /*
    int h;
    SDL_QueryTexture(image, NULL, NULL, NULL, &h);
    */
    if (image)
        return image->h;
    return -1;

}

GPU_Image *Sprite::getImage() {

    return image;

}

Vector2 Sprite::getPosition() {

    return Vector2(getCenterX(), getCenterY());

}

void Sprite::setImage(GPU_Image *image) {

    this->image = image;

}

void Sprite::setDstX(float posX) {

    dst.x = posX;

}

void Sprite::setDstY(float posY) {

    dst.y = posY;

}

void Sprite::setDstW(float sizeW) {

    dst.w = sizeW;

}

void Sprite::setDstH(float sizeH) {

    dst.h = sizeH;

}

// Set the dst rect based in another rect. If the parameter is NULL dst will fill the screen.
void Sprite::setDst(GPU_Rect *other) {

    if (!other) {
        setDst(0, 0, Configuration::get().SCREEN_W, Configuration::get().SCREEN_H);
    }
    else {
        dst.x = other->x;
        dst.y = other->y;
        dst.w = other->w;
        dst.h = other->h;
    }
}

void Sprite::setDst(float x, float y, float w, float h) {

    this->dst.x = x;
    this->dst.y = y;
    this->dst.w = w;
    this->dst.h = h;

}

void Sprite::setSrcX(float posX) {

    src.x = posX;

}

void Sprite::setSrcY(float posY) {

    src.y = posY;

}

void Sprite::setSrcW(float sizeW) {

    src.w = sizeW;

}

void Sprite::setSrcH(float sizeH) {

    src.h = sizeH;

}

void Sprite::setSrc(float x, float y, float w, float h) {

    this->src.x = x;
    this->src.y = y;
    this->src.w = w;
    this->src.h = h;

}

void Sprite::setSrc(GPU_Rect *other) {

    if (!other) {
        if (!image) {
            SDL_Log("Cannot use the default size: Sprite has no image.");
            return;
        }
        else {
            setSrc(0, 0, getImage()->w, getImage()->h);
        }
    }
    else {
        setSrc(other->x, other->y, other->w, other->h);
    }

}

void Sprite::setCenter(float x, float y) {

    setCenterX(x);
    setCenterY(y);

}

void Sprite::setCenter(Vector2 parentCenter) {

    setCenter(parentCenter.x, parentCenter.y);

}

void Sprite::translate(Vector2 *vec) {

    dst.x += vec->x;
    dst.y += vec->y;

}

void Sprite::setCenterX(float x) {

    dst.x = x - dst.w / 2;

}

void Sprite::setCenterY(float y) {

    dst.y = y - dst.h / 2;

}

void Sprite::flip() {

    flipped = !flipped;

}

bool Sprite::isFlipped() {

    return flipped;

}

void Sprite::setHidden(bool state) {

    hidden = state;

}

bool Sprite::isHidden() {

    return hidden;

}

void Sprite::cleanUp() {

    image = NULL;
    owner = NULL;

}

void Sprite::accept(RenderingSystem *system) {

    system->render(this);

}
