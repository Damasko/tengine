#include "graphiccomponent.hpp"
#include "Core/Systems/RenderingSystem/renderingsystem.hpp"

GraphicComponent::GraphicComponent(GameObject *owner) : Component(owner) {

    systems = SYS_RENDERING;

}


void GraphicComponent::init() {

}

void GraphicComponent::update() {

}

void GraphicComponent::fixedUpdate() {


}

void GraphicComponent::onExit() {

}

void GraphicComponent::reset() {

}



