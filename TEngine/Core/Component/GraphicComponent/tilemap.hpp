#ifndef TILEMAP_H
#define TILEMAP_H

#include <algorithm>

#include "Core/Texture/tileset.hpp"
#include "Core/Config/configuration.hpp"
#include "Core/Component/Camera/camera2D.hpp"
#include "Core/Component/GraphicComponent/graphiccomponent.hpp"

/*  First 8 bits unused       0  0  0  0  0  0  0  0
 *  (Currently)               |  |  |  |  |  |  |  +-> VISIBLE
 *                            |  |  |  |  |  |  +-> SOLID
 *                            |  |  |  |  |  +-> DESTRUCTABLE
 *                            |  |  |  |  +-> EMÏSION ?
 *                            |  |  |  +-> IS_TRIGGERED
 *                            |  |  +-> ANIMATED
 *                            |  +-> CUSTOM_PROP3
 *                            +-> CUSTOM_PROP4

 *
*/

const short   IS_VISIBLE        = 1;
const short   IS_SOLID          = 2;
const short   IS_DESTRUCTABLE   = 4;
const short   HAS_EMISSION      = 8;
const short   IS_TRIGGERED      = 16;


struct Tile {

    int             id = 0;
    unsigned short  properties = 0;
    Vector2         position;
    GPU_Rect        src = { 0, 0, 128, 128 };

    bool getProperty(const short flag) {

        return (properties & flag) == flag;

    }

};

class Tilemap : public GraphicComponent {

    private:

        //Container::Matrix map;
        GPU_Rect            area;
        std::vector<Tile>   tiles;
        Tileset             tileset;

        int                 mapW;
        int                 mapH;
        int                 dstW;
        int                 dstH;

    public:

        Tilemap(GameObject *owner);
        ~Tilemap();

        void init();

        int loadFromFile(std::string mapName, std::string path="");
        int loadTileset(GPU_Image *texture, std::string name);
        int load(std::string mapName, std::string tilesetName, GPU_Image *tilesetImg);
        std::vector<Tile> *getTiles();
        std::vector<Tile*> getVisibleTiles(Camera2D *camera);

        void setMapSize(Vector2 size);
        void setBaseTileSize(Vector2 size);

        void setPosition(Vector2 newPos);

        Tile *getTile(int x, int y);
        Tile *getTileFromPosition(Vector2 position);
        std::vector<Tile*> getTilesArea(GPU_Rect *target);
        Tileset *getTileset();
        GPU_Rect *getDstRect();
        int getMapW();
        int getMapH();
        int getTileW();
        int getTileH();
        void setTile(Tile *target);
        void cleanUp();

        void accept(RenderingSystem *system);

};

#endif // TILEMAP_H
