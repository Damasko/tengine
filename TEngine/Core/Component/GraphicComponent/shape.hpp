#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <vector>
#include "graphiccomponent.hpp"

enum ShapeType {

    CIRCLE,
    RECTANGLE,
    POLYGON

};


class Shape : public GraphicComponent {

    private:

        ShapeType           type;
        bool                filled;
        std::vector<float>  values;

    public:

        Shape(GameObject *owner);
        ShapeType getType();
        bool isFilled();
        std::vector<float> *getValues();

};

#endif // SHAPE_HPP
