#ifndef GRAPHICCOMPONENT_HPP
#define GRAPHICCOMPONENT_HPP

#include "Core/Component/Base/component.hpp"
#include "SDL_gpu.h"

class RenderingSystem;

class GraphicComponent : public Component {

    protected:



    public:

        GraphicComponent(GameObject *owner);
        virtual ~GraphicComponent() { }
        virtual void    init();
        virtual void    update();
        virtual void    fixedUpdate();
        virtual void    onExit();
        virtual void    reset();
        virtual void    cleanUp() = 0;
        virtual void    accept(RenderingSystem *system) = 0;
        virtual GPU_Rect   *getDstRect() = 0;

};

#endif // GRAPHICCOMPONENT_HPP
