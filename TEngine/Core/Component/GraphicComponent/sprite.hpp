#ifndef SPRITE_H
#define SPRITE_H

#include "SDL.h"
#include "SDL_gpu.h"
#include "Core/Math/vector2.hpp"
#include "Core/Config/configuration.hpp"
#include "Core/Component/GraphicComponent/graphiccomponent.hpp"

//class Renderer;

class Sprite : public GraphicComponent {

    protected:

        GPU_Image           *image;
        GPU_Rect            src;
        GPU_Rect            dst;
        bool                flipped;
        bool                hidden;

    public:

        Sprite(GameObject *owner);
        Sprite(GameObject *owner, float w, float h);
        ~Sprite();
        static bool collide(Sprite *a, Sprite *b);

        float getLeft();
        float getRight();
        float getTop();
        float getBottom();
        float getCenterX();
        float getCenterY();
        Vector2 getCenter();
        Vector2 getPosition();

        int getImageW();
        int getImageH();

        void setImage(GPU_Image *image);

        void setDstX(float posX);
        void setDstY(float posY);
        void setDstW(float sizeW);
        void setDstH(float sizeH);
        void setDst(GPU_Rect *other);
        void setDst(float x, float y, float w, float h);

        void setSrcX(float posX);
        void setSrcY(float posY);
        void setSrcW(float sizeW);
        void setSrcH(float sizeH);
        void setSrc(float x, float y, float w, float h);
        void setSrc(GPU_Rect *other);

        void setCenter(float x, float y);
        void setCenter(Vector2 parentCenter);
        void setCenterX(float x);
        void setCenterY(float y);

        void translate(Vector2 *vec);

        GPU_Rect *getDstRect();
        GPU_Rect *getSrcRect();
        GPU_Image *getImage();

        void flip();
        bool isFlipped();
        void setHidden(bool state);
        bool isHidden();

        void cleanUp();

        void accept(RenderingSystem *system);

};

#endif // SPRITE_H
