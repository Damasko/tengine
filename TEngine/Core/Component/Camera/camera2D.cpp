#include "camera2D.hpp"
#include "Core/GameObject/gameobject.hpp"

Camera2D::Camera2D(GameObject *owner) : InputComponent(owner), followOwner(true) {

    systems = SYS_INPUT /* & SYS_RENDERING */;

    speed.x = 800.0;
    speed.y = 800.0;

    visibleArea.x = 0;
    visibleArea.y = 0;
    visibleArea.w = Configuration::get().SCREEN_W;
    visibleArea.h = Configuration::get().SCREEN_H;

    gpuCamera = GPU_GetDefaultCamera();
    setAction("CAMERA_FOLLOW");
    setCenter(owner->getTransform()->getPosition2D());
    name = "Camera2D";

}

Camera2D::~Camera2D() {


}

/*
void Camera2D::init(GPU_Target *target) {

    this->target = target;
    GPU_SetCamera(target, &gpuCamera);

}
*/

void Camera2D::setW(float w) {

    visibleArea.w = w;

}

void Camera2D::setH(float h) {

    visibleArea.h = h;

}

void Camera2D::setCenterX(float x) {

    visibleArea.x = x - (visibleArea.w / 2);

}

void Camera2D::setCenterY(float y) {

    visibleArea.y = y - (visibleArea.h / 2);

}

void Camera2D::setCenter(Vector2 center) {

    setCenterX(center.x);
    setCenterY(center.y);

}

float Camera2D::getCenterX() {

    return visibleArea.x + (visibleArea.w / 2);

}

float Camera2D::getCenterY() {

    return visibleArea.y + (visibleArea.h / 2);

}
float Camera2D::getX() {

    return visibleArea.x;

}

float Camera2D::getY() {

    return visibleArea.y;

}

Vector2 Camera2D::getCenter() {

    return Vector2(getCenterX(), getCenterY());

}

/*
void Camera2D::followTarget(Sprite &target, float offsetX, float offsetY) {

    Vector2 targetPos = target.getPosition();
    Vector2 goTo = Vector2(0.0f, 0.0f);
    if ((getCenterX() - targetPos.x) >= offsetX) {
        goTo.x -= speed.x;

    }

    else if ((getCenterX() - targetPos.x) < offsetX) {
        goTo.x += speed.x;

    }

    if (abs(getCenterY() - targetPos.y) >= offsetX) {
        goTo.y -= speed.y;

    }

    else if (abs(getCenterY() - targetPos.y) < offsetX) {
        goTo.y += speed.y;

    }

    move(&goTo);

}

*/

void Camera2D::move(Vector2 *where) {

    visibleArea.x += speed.x * where->x * Timer::deltaTime;
    visibleArea.y += speed.y * where->y * Timer::deltaTime;

}

void Camera2D::setGPUCamera(GPU_Camera cam) {

    gpuCamera = cam;

}

GPU_Camera *Camera2D::getGPUCamera() {

    return &gpuCamera;

}

GPU_Rect *Camera2D::getRect() {

    return &visibleArea;

}

void Camera2D::setZoom(float zoom) {

    /*
    Vector2 center = Vector2(visibleArea.x + (visibleArea.w / 2), visibleArea.y + (visibleArea.h / 2));
    visibleArea.w -= 2 * zoom * Timer::deltaTime;
    visibleArea.h -= 2 * zoom * Timer::deltaTime;
    visibleArea.x = center.x - (visibleArea.w / 2);
    visibleArea.y = center.y - (visibleArea.h / 2);
    */

    gpuCamera.zoom += zoom * Timer::deltaTime;
    GPU_SetCamera(target, &gpuCamera);

}

void Camera2D::init() {


}

void Camera2D::update(){

}

void Camera2D::fixedUpdate() {

}

void Camera2D::onExit() {

}

void Camera2D::reset() {

}

void Camera2D::cleanUp() {


}
