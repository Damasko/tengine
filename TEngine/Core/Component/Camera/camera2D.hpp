#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "SDL_gpu.h"
#include "Core/Math/vector2.hpp"
#include "Core/Component/InputComponent/inputcomponent.hpp"
#include "Core/Config/configuration.hpp"
#include "Core/Timer/timer.hpp"

class Camera2D : public InputComponent {

    private:

        GPU_Rect    visibleArea;
        GPU_Camera  gpuCamera;
        Vector2     speed;
        GPU_Target  *target;
        bool        followOwner;

    public:

        Camera2D(GameObject *owner);
        ~Camera2D();
        //void init(GPU_Target *target);
        void setW(float w);
        void setH(float h);
        void setCenterX(float x);
        void setCenterY(float y);
        void setCenter(Vector2 center);
        float getCenterX();
        float getCenterY();
        float getX();
        float getY();
        Vector2 getCenter();

        //void followTarget(Sprite &target, float offsetX=0, float offsetY=0);

        void move(Vector2 *where);
        void setGPUCamera(GPU_Camera cam);
        GPU_Camera *getGPUCamera();
        GPU_Rect *getRect();
        void setZoom(float zoom);

        void init();
        void update();
        void fixedUpdate();
        void onExit();
        void reset();
        void cleanUp();


};

#endif // CAMERA_HPP
