#ifndef FOLLOWMOUSE_HPP
#define FOLLOWMOUSE_HPP

#include "Core/Component/InputComponent/inputcomponent.hpp"

class FollowMouse : public InputComponent {

    private:

        std::string action;

    public:

        FollowMouse(GameObject *owner);
        ~FollowMouse();
        void cleanUp();

};

#endif // FOLLOWMOUSE_HPP
