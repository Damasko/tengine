#ifndef INPUTCOMPONENT_HPP
#define INPUTCOMPONENT_HPP

#include "Core/Component/Base/component.hpp"

class InputSystem;

class InputComponent : public Component {

    private:

        std::string     action;

    public:

        InputComponent(GameObject *owner);
        virtual ~InputComponent() { }
        virtual void init();
        virtual void update();
        virtual void fixedUpdate();
        virtual void onExit();
        virtual void reset();
        virtual void cleanUp();

        //virtual void accept(InputSystem *system);
        void setAction(std::string action);
        std::string getAction();


};

#endif // INPUTCOMPONENT_HPP
