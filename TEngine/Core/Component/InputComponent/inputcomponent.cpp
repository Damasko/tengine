#include "inputcomponent.hpp"

InputComponent::InputComponent(GameObject *owner) : Component(owner) {

    systems = SYS_INPUT;

}

void InputComponent::init() {

}

void InputComponent::update() {

}

void InputComponent::fixedUpdate() {

}

void InputComponent::onExit() {

}

void InputComponent::reset() {

}

void InputComponent::cleanUp() {

}

std::string InputComponent::getAction() {

    return action;

}

void InputComponent::setAction(std::string action) {

    this->action = action;

}



