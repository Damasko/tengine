#include "regions.hpp"

Regions::Regions() {

}

void Regions::addRegion(std::string regionName, Region region) {

    auto it = regions.find(regionName);

    if (it == regions.end()) {

        regions.insert(std::make_pair(regionName, region));

    }

    else {

        it->second = region;

    }


}

int Regions::loadFromFile(std::string fileName) {

    Message data;

    if (data.loadFromFile(fileName) < 0) {

        return -1;

    }

    std::vector<std::pair<std::string, std::string> > values = data.getAllStrings();

    for (unsigned int i = 0; i < values.size(); ++i) {

        Region reg;
        std::vector<std::string> strData = StringTools::splitString(values[i].second.c_str());

        // Src rectangle params

        reg.src.x = StringTools::to_integer(strData[0]);
        reg.src.y = StringTools::to_integer(strData[1]);
        reg.src.w = StringTools::to_integer(strData[2]);
        reg.src.h = StringTools::to_integer(strData[3]);

        // Properties param is optional

        if (strData.size() > 4)
            reg.properties = StringTools::to_integer(strData[4]);
        else
            reg.properties = 0;

        addRegion(values[i].first, reg);

    }

    return 0;

}

std::vector<Region> Regions::getRegions() {

    std::vector<Region> res;

    for (auto it = regions.begin(); it != regions.end(); ++it) {

        res.emplace_back(it->second);

    }

    return res;

}

Region *Regions::getRegion(std::string id) {

    std::unordered_map<std::string, Region>::iterator it = regions.find(id);
    if (it == regions.end()) {

        SDL_Log("ERROR: Region \"%s\" is not in the map", id.c_str());
        SDL_Log("Debug: Regions avaibles are:");
        printRegions();
        return NULL;

    }

    return &it->second;

}

void Regions::cleanUp() {

    regions.clear();

}

void Regions::printRegions() {

    for (auto it = regions.begin(); it != regions.end(); ++it) {

        SDL_Log("Region name:   %s", it->first.c_str());

    }

}
