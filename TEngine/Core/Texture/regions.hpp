#ifndef REGIONS_H
#define REGIONS_H

#include <unordered_map>
#include <string>
//#include "tinyxml.h"
#include "SDL_gpu.h"
#include "Core/Message/message.hpp"

struct Region {

    int         id;
    int         properties;
    GPU_Rect    src;

};

class Regions {

    private:

        std::unordered_map<std::string, Region> regions;

    public:

        Regions();
        void addRegion(std::string regionName, Region region);
        int loadFromFile(std::string fileName);
        std::vector<Region> getRegions();
        Region *getRegion(std::string id);
        void cleanUp();

        // Debug
        void printRegions();

};

#endif // REGIONS_H
