#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include "SDL_gpu.h"

class Texture {

    private:

        GPU_Image       *texture;

    public:

        Texture();
        GPU_Image *getTexture();

};

#endif // TEXTURE_HPP
