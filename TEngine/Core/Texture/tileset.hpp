#ifndef TILESET_HPP
#define TILESET_HPP

#include "Core/Texture/regions.hpp"
#include "Core/Message/message.hpp"
#include "Core/Config/configuration.hpp"

class Tileset {

    private:

        std::string     name;
        GPU_Image       *texture;
        GPU_Image       *normals;
        bool            hasNormals;
        Regions         regions;

    public:

        Tileset();
        void setImage(GPU_Image *tex);
        GPU_Image *getImage();
        int load(std::string fileName);
        int loadAll(std::string fileName, std::string key,  GPU_Image *image);
        std::string getName();
        void setName(std::string newName);
        Region *getRegion(std::string keyRegion);
        void cleanUp();

        //Debug
        void printRegionsNames();

        void switchToNormalsImage();


};

#endif // TILESET_HPP
