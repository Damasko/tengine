#include "tileset.hpp"

Tileset::Tileset() : name(""), texture(NULL), normals(NULL), hasNormals(false) {

}

void Tileset::setImage(GPU_Image *tex) {

    texture = tex;

}

GPU_Image *Tileset::getImage() {

    return texture;

}

int Tileset::load(std::string fileName) {

   if (regions.loadFromFile(Configuration::get().assetFolder + Configuration::get().imagesFolder + fileName + ".region") < 0) {
        SDL_Log("Cannot load the regions from the file %s", fileName.c_str());
        return -1;
   }

   setName(fileName);

   return 0;

}

int Tileset::loadAll(std::string fileName, std::string key, GPU_Image *image) {

    if (!image) {
        SDL_Log("Cannot load tileset image");
        return -1;
    }

    if (load(fileName) < 0) {
        return -2;

    }

    setImage(image);

    return 0;

}

std::string Tileset::getName() {

    return name;

}

void Tileset::setName(std::string newName) {

    name = newName;

}

Region *Tileset::getRegion(std::string keyRegion) {

     return regions.getRegion(keyRegion);

}

void Tileset::cleanUp() {

    regions.cleanUp();
    texture = NULL;

}

void Tileset::printRegionsNames() {

    regions.printRegions();

}

void Tileset::switchToNormalsImage() {

    if (hasNormals or !normals) {
        SDL_Log("Tileset Error: Normals image is NULL");
        return;
    }

    GPU_Image *aux = normals;
    normals = texture;
    normals = aux;

}
