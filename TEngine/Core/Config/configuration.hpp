#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP


#include "Core/Message/message.hpp"

class Configuration {

    private:

        std::string             programName;
        std::string             company;

        std::string             configFolder;

        int                     soundVolume;
        int                     musicVolume;

    public:

        std::string             assetFolder;
        std::string             imagesFolder;
        std::string             mapsFolder;
        std::string             savedFolder;
        std::string             soundsFolder;
        std::string             fontsFolder;
        std::string             shadersFolder;
        std::string             gObjectsFolder;
        std::string             sceneFolder;
        std::string             dataFolder;

        int                     SCREEN_W;
        int                     SCREEN_H;

        Message                 properties;

    public:

        Message *getMessage();

        static Configuration &get() {

            static Configuration *instance = new Configuration();
            return *instance;

        }

    private:

        Configuration() {

            loadConfiguration();

        }

        int loadConfiguration();
        virtual int generateFiles();


};

#endif // CONFIGURATION_HPP
