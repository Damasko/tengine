#include "configuration.hpp"


int Configuration::loadConfiguration() {

    SCREEN_W        = 1024;
    SCREEN_H        = 640;
    assetFolder     = "Assets/";
    imagesFolder    = "Images/";
    mapsFolder      = "Maps/";

    soundsFolder    = "Sounds/";
    fontsFolder     = "Fonts/";
    shadersFolder   = "Shaders/";
    gObjectsFolder  = "Gameobjects/";
    sceneFolder     = "Scenes/";
    dataFolder      = "Data/";

    Message companyAndProgram;
    if (companyAndProgram.loadFromFile(assetFolder + "company", false) < 0) {
        SDL_Log("Warning: Company and program name does not exist, please create a company file. Using default folder name...");
        company         = "defaultCompany";
        programName     = "DefaultProgram";
    }
    else {
        company         = companyAndProgram.getString("company");
        programName     = companyAndProgram.getString("program");
    }

    char * gameFolder = SDL_GetPrefPath(company.c_str(), programName.c_str());

    savedFolder = std::string(gameFolder);

    free(gameFolder);
    if (properties.loadFromFile(savedFolder + "config.cfg", false) < 0) {

        generateFiles();

    }

    properties.loadFromFile(savedFolder + "config.cfg");
    properties.insertString("asset_folder", assetFolder);
    properties.insertString("images_folder", imagesFolder);
    properties.insertString("maps_folder", mapsFolder);
    properties.insertString("saved_folder", savedFolder);
    properties.insertString("sounds_folder", soundsFolder);
    properties.insertString("fonts_folder", fontsFolder);
    properties.insertString("shaders_folder", shadersFolder);
    properties.insertString("data_folder", dataFolder);
    SCREEN_W = properties.getInt("screen_w");
    SCREEN_H = properties.getInt("screen_h");

    return 0;

}

Message* Configuration::getMessage() {

    return &properties;

}

int Configuration::generateFiles() {

    Message newConfig;
    newConfig.insertInt("sound_vol", 50);
    newConfig.insertInt("music_vol", 50);
    newConfig.insertInt("screen_w", 1024);
    newConfig.insertInt("screen_h", 640);
    newConfig.writeToFile(savedFolder + "config.cfg");

    Message slot;

    Message slot1, slot2, slot3;
    if (slot1.loadFromFile(savedFolder + "game.001") < 0) {
        slot.writeToFile(savedFolder + "game.001");
    }
    if (slot2.loadFromFile(savedFolder + "game.002") < 0) {
        slot.writeToFile(savedFolder + "game.002");
    }
    if (slot3.loadFromFile(savedFolder + "game.003") < 0) {
        slot.writeToFile(savedFolder + "game.003");
    }
    return 0;

}
