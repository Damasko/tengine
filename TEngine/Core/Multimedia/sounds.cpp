#include "sounds.hpp"


int Sounds::chunkVolume = 100;
int Sounds::musicVolume = 100;

Sounds::Sounds() {


}

int Sounds::init() {

    std::string path = Configuration::get().assetFolder + Configuration::get().soundsFolder;
/*
    loadSound(path + "hit.wav", "hit_block");
    loadSound(path + "hitRacket.wav", "hit_racket");
    loadSound(path + "electricity.wav", "hit_protection");
    loadSound(path + "cannon.wav", "cannon");
    loadSound(path + "frozen.wav", "frozen");
    loadSound(path + "pickmoney.wav", "pickmoney");
    loadSound(path + "explosion2.wav", "explosion");
    loadSound(path + "horn.wav", "horn");
*/
    return 0;

}

int Sounds::loadSound(std::string soundPath, std::string soundName) {

    std::unordered_map<std::string, Mix_Chunk*>::iterator it = sounds.find(soundName);
    Mix_Chunk *chunk = NULL;
    chunk = Mix_LoadWAV(soundPath.c_str());

    if (!chunk) {

        SDL_Log("Cannot load the sound: %s", soundPath.c_str());
        return -1;

    }


    if (it == sounds.end()) {

        sounds.insert(std::make_pair(soundName, chunk));

    }

    else {
        Mix_FreeChunk(it->second);
        it->second = chunk;
    }

    return 0;

}

int Sounds::loadMusic(std::string musicPath, std::string musicName) {

    std::unordered_map<std::string, Mix_Music*>::iterator it = musics.find(musicName);
    Mix_Music *music = NULL;
    music = Mix_LoadMUS(musicPath.c_str());

    if (!music) {

        SDL_Log("Cannot load the music: %s", musicName.c_str());
        return -1;

    }

    if (it == musics.end()) {

        musics.insert(std::make_pair(musicName, music));

    }

    else {
        Mix_FreeMusic(it->second);
        it->second = music;
    }

    return 0;


}

Mix_Chunk *Sounds::getSound(std::string keyName) {

    std::unordered_map<std::string, Mix_Chunk*>::iterator it = sounds.find(keyName);
    if (it == sounds.end()) {
        SDL_Log("Error: Sound not found");
        return NULL;
    }
    return it->second;

}

Mix_Music *Sounds::getMusic(std::string keyName) {

    std::unordered_map<std::string, Mix_Music*>::iterator it = musics.find(keyName);
    if (it == musics.end()) {
        SDL_Log("Error: Music not found");
        return NULL;
    }
    return it->second;


}

void Sounds::setChunkVolume(Mix_Chunk *target, int volume) {

    Mix_VolumeChunk(target, volume);

}

void Sounds::setSoundsVolume(int volume) {
/*
    for (int i = 0; i < sounds.size(); ++i) {

        setChunkVolume(sounds[i], volume);

    }
*/

    std::unordered_map<std::string, Mix_Chunk*>::iterator it;
    for(it = sounds.begin(); it != sounds.end(); ++it) {
        setChunkVolume(it->second, volume);

    }

    chunkVolume = volume;

}

void Sounds::setMusicsVolume(int volume) {

   MathTools::clamp(volume, 0, 100);
   float conversion = 100 / MIX_MAX_VOLUME;

   int vol = volume * conversion;

   Mix_VolumeMusic(vol);

}

void Sounds::muteSounds() {



}

void Sounds::cleanUp() {
/*
    for (int i = 0; i < sounds.size(); ++i) {
        Mix_FreeChunk(sounds[i]);
    }
    for (int i = 0; i < musics.size(); ++i) {
        Mix_FreeMusic(musics[i]);
    }
*/

    std::unordered_map<std::string, Mix_Chunk*>::iterator it;
    for(it = sounds.begin(); it != sounds.end(); ++it) {
        Mix_FreeChunk(it->second);

    }

    std::unordered_map<std::string, Mix_Music*>::iterator itm;
    for(itm = musics.begin(); itm != musics.end(); ++itm) {
        Mix_FreeMusic(itm->second);

    }

}

int Sounds::getVolume() {

    return chunkVolume;

}
