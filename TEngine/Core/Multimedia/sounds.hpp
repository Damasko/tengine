#ifndef SOUNDS_H
#define SOUNDS_H

#include <string>
//#include <vector>
#include <unordered_map>
#include "SDL.h"
#include "SDL_mixer.h"
#include "Core/Config/configuration.hpp"
#include "Core/Math/mathtools.hpp"

class Sounds {

    public:

        static int      chunkVolume;
        static int      musicVolume;
        std::unordered_map<std::string, Mix_Chunk*> sounds;
        std::unordered_map<std::string, Mix_Music*> musics;

    public:
        Sounds();
        virtual int init();
        int loadSound(std::string soundPath, std::string soundName);
        int loadMusic(std::string musicPath, std::string musicName);
        Mix_Chunk *getSound(std::string keyName);
        Mix_Music *getMusic(std::string keyName);
        static void setChunkVolume(Mix_Chunk *target, int volume);
        void setSoundsVolume(int volume);
        void setMusicsVolume(int volume);
        void muteSounds();
        void cleanUp();
        int getVolume();
};

#endif // SOUNDS_H
