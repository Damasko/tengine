#ifndef MULTIMEDIA_H
#define MULTIMEDIA_H


#include "images.hpp"
#include "sounds.hpp"


class Multimedia {

    public:

        Images      images;
        Sounds      sounds;
        /*
        Font        font;
        Shaders     shaders;
        */

    public:
        Multimedia();
        virtual int init();
        void cleanUp();
};

#endif // MULTIMEDIA_H
