#ifndef IMAGES_H
#define IMAGES_H

#include <vector>
#include <unordered_map>
#include <string>
#include "SDL.h"
#include "SDL_gpu_GLES_2.h"


#include "Core/Config/configuration.hpp"

#include "Core/Texture/tileset.hpp"

class Images {

    private:

        std::string                                 folder;
        std::unordered_map<std::string, GPU_Image*> images;
        std::unordered_map<std::string, Tileset>    tilesets;

    public:

        Images();
        virtual int init();
        int loadImage(std::string imagePath, std::string imageName);
        int unloadImage(std::string keyName);
        GPU_Image *getImage(std::string keyName);
        GPU_Image *lazyLoad(std::string imagePath);

        Tileset *loadTileset(std::string name, std::string path="", GPU_FileFormatEnum fileFormat=GPU_FILE_PNG);
        void addTileset(std::string key, Tileset tileset);
        Tileset *getTileset(std::string key);

        void cleanUp();

    public:

        static int saveImage(GPU_Image *img, std::string fileName, GPU_FileFormatEnum fileFormat) {

            if (!GPU_SaveImage(img, fileName.c_str(), fileFormat)) {
                SDL_Log("Cannot load the image!");
                return -1;
            }

            return 0;

        }

        int loadImagesFromFile(std::string fileName);

        void printTilesetsInMap();

};

#endif // IMAGES_H
