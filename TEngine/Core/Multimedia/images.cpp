#include "images.hpp"

Images::Images() {

}

int Images::init() {

    folder = Configuration::get().assetFolder + Configuration::get().imagesFolder;

    return 0;

}

int Images::loadImage(std::string imagePath, std::string imageName) {

    std::unordered_map<std::string, GPU_Image*>::iterator it = images.find(imageName);
    GPU_Image *image = NULL;
    image = GPU_LoadImage((folder + imagePath).c_str());

    if (!image) {

        SDL_Log("Cannot load the image: %s. Please check if %s is a valid path and image filename", imageName.c_str(), imagePath.c_str());
        return -1;

    }
    if (it == images.end()) {

        images.insert(std::make_pair(imageName, image));
    }
    else {
        GPU_FreeImage(it->second);
        it->second = image;
    }

    return 0;

}

int Images::unloadImage(std::string keyName) {

    std::unordered_map<std::string, GPU_Image*>::iterator it;
    it = images.find(keyName);

    if (it == images.end())
        return -1;

    GPU_FreeImage(it->second);
    images.erase(it);
    return 0;

}

// Note: the image is owned by you with "lazyLoad" and you are responsible to free the resource

GPU_Image *Images::lazyLoad(std::string imagePath) {

    GPU_Image *image = NULL;
    image = GPU_LoadImage(imagePath.c_str());
    if (!image) {
        //SDL_Log("There was a problem while loading the image: %s . WARNING: The image is NULL", IMG_GetError());
        SDL_Log("Cannot load the image: %s", GPU_GetErrorString(GPU_ERROR_DATA_ERROR));
        return NULL;
    }

    return image;

}

GPU_Image *Images::getImage(std::string keyName) {

    std::unordered_map<std::string, GPU_Image*>::iterator it = images.find(keyName);
    if (it == images.end()) {
        return NULL;
    }
    return it->second;

}

// Poco eficiente ya que hace una copia del Tileset y al final una búsqueda en el map, valorar el uso de punteros
//en el map en vez de hacer copias. Tiene sentido devolver el puntero al tileset almacenado si todo ha ido bien o NULL en caso de error
// Así el usuario puede usarlo inmediatamente

Tileset* Images::loadTileset(std::string name, std::string path, GPU_FileFormatEnum fileFormat) {

    if (path.empty())
        path = Configuration::get().assetFolder + Configuration::get().imagesFolder;

    Tileset tileset;

    std::string fileFix = name;

    // Ouch! fix this dirty code
    if (fileFormat == GPU_FILE_BMP) {
        fileFix += ".bmp";
    }
    else if (fileFormat == GPU_FILE_TGA) {
        fileFix += ".tga";
    }
    else {
        fileFix += ".png";
    }

    if (loadImage(fileFix, name) < 0) {

        SDL_Log("Tileset image file not found in path \"%s\" with key given \"%s\"", path.c_str(), name.c_str());
        return NULL;

    }


    if (tileset.loadAll(name, name, getImage(name)) < 0) {
        SDL_Log("Cannot load tileset");
        return NULL;
    }

    addTileset(name, tileset);

    return getTileset(name);

}

void Images::addTileset(std::string key, Tileset tileset) {

    auto it = tilesets.find(key);

    if (it == tilesets.end()) {

        tilesets.insert(std::make_pair(key, tileset));

    }
    else {

        it->second.cleanUp();
        it->second = tileset;

    }

}

Tileset *Images::getTileset(std::string key) {

    auto it = tilesets.find(key);

    if (it == tilesets.end()) {
        SDL_Log("Tileset not in map");
        return NULL;
    }

    return &it->second;

}

void Images::cleanUp() {

    for(auto it = images.begin(); it != images.end(); ++it) {
        GPU_FreeImage(it->second);

    }

    for (auto it = tilesets.begin(); it != tilesets.end(); ++it) {

        it->second.cleanUp();

    }

}

int Images::loadImagesFromFile(std::string fileName) {

    //Message
    return 0;

}

void Images::printTilesetsInMap() {

    for (auto it = tilesets.begin(); it != tilesets.end(); ++it) {

        SDL_Log(it->first.c_str());

    }

}
