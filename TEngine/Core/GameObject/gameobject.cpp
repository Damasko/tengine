#include "gameobject.hpp"

int GameObject::ID = 0;

GameObject::GameObject() : transform(this), layer(0), name("") {

    transform.init();
    //components.emplace_back(&transform);  // DA ERROR, NO PUEDES METER ESTE COMPONENTE CON LOS DEMAS QUE VAN A LIBERARSE MEMORIA, ESTE NO USA
                                            // MEMORIA DINAMICA
    ++ID;

}

Transform *GameObject::getTransform() {

    return &transform;

}

void GameObject::addComponent(Component *c) {

    if (!c) {
        SDL_Log("GameObject Error: NULL component");
        return;
    }

    components.emplace_back(c);

}

void GameObject::setLayer(int nlayer) {

    layer = nlayer;

}

int GameObject::getLayer() {

    return layer;

}

void GameObject::setID(int nID) {

    ID = nID;

}

int GameObject::getID() {

    return ID;

}


void GameObject::setName(std::string nName) {

    name = nName;

}

std::string GameObject::getName() {

    return name;

}

std::vector<Component*> *GameObject::getComponents() {

    return &components;

}

void GameObject::cleanUp() {

    for (unsigned int i = 0; i < components.size(); ++i) {
        components[i]->cleanUp();
        delete components[i];
        components[i] = NULL;
    }

    components.clear();

}

void GameObject::addChild(GameObject *child) {

    if (!child) {
        SDL_Log("Cannot emplace child: Child is NULL");
        return;
    }

    childs.emplace_back(child);

}

std::vector<GameObject*> GameObject::getChildren() {

    return childs;

}

