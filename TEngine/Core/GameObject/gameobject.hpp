#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include <vector>
#include "Core/Component/Transform/transform.hpp"

class GameObject {

    private:

        std::string             name;
        Transform               transform;
        std::vector<Component*> components;
        std::vector<GameObject*>childs;

        int                     layer;
        static int              ID;

    public:

        GameObject();
        void addComponent(Component *c);
        Transform *getTransform();
        void setLayer(int nlayer);
        int getLayer();

        void setID(int nID);
        int getID();

        void setName(std::string nName);
        std::string getName();

        std::vector<Component *> *getComponents();
        void cleanUp();
        void addChild(GameObject *child);
        std::vector<GameObject*> getChildren();

};

#endif // GAMEOBJECT_HPP
