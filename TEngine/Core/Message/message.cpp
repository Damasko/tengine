#include "message.hpp"

Message::Message() {

}

void Message::insertInt(std::string valueName, int value) {

    std::unordered_map<std::string, int>::iterator it = dataInt.find(valueName);
    if (it == dataInt.end()) {

        dataInt.insert(std::make_pair(valueName, value));

    }
    else {

        it->second = value;

    }

}

void Message::insertString(std::string valueName, std::string value) {

    std::unordered_map<std::string, std::string>::iterator it = dataString.find(valueName);
    if (it == dataString.end()) {
        dataString.insert(std::make_pair(valueName, value));
    }
    else {
        it->second = value;
    }

}

void Message::insertBool(std::string valueName, bool value) {

    auto it = dataBool.find(valueName);

    if (it == dataBool.end()) {

        dataBool.insert(std::make_pair(valueName, value));
    }
    else {
        it->second = value;
    }

}

void Message::insertLong(std::string valueName, long value) {

    auto it = dataLong.find(valueName);

    if (it == dataLong.end()) {
        dataLong.insert(std::make_pair(valueName, value));

    }
    else {
        it->second = value;

    }


}

void Message::retrieveInt(std::string valueName, int &fillValue) {

    std::unordered_map<std::string, int>::iterator it = dataInt.find(valueName);
    if (it == dataInt.end()) {
        SDL_Log("ERROR: Integer value %s does not exist", valueName.c_str());
        fillValue = NULL;
    }
    else {
        fillValue = it->second;
    }
}

void Message::retrieveLong(std::string valueName, long &fillValue) {

    std::unordered_map<std::string, long>::iterator it = dataLong.find(valueName);
    if (it == dataLong.end()) {
        SDL_Log("ERROR: Long value %s does not exist", valueName.c_str());
        fillValue = NULL;
    }
    else {
        fillValue = it->second;
    }

}

void Message::retrieveBool(std::string valueName, bool *fillValue) {

    std::unordered_map<std::string, bool>::iterator it = dataBool.find(valueName);
    if (it == dataBool.end()) {

        fillValue = NULL;

    }

    else {

        *fillValue = it->second;

    }

}

int Message::getInt(std::string valueName) {
    std::unordered_map<std::string, int>::iterator it = dataInt.find(valueName);

    if (it == dataInt.end()) {
        SDL_Log("ERROR: Int value %s does not exist", valueName.c_str());
        return -1;
    }
    return it->second;

}

std::string Message::getString(std::string valueName) {

    std::unordered_map<std::string, std::string>::iterator it = dataString.find(valueName);
    if (it == dataString.end()) {
        SDL_Log("ERROR: String value %s does not exist", valueName.c_str());
        return "";
    }
    return it->second;

}

bool Message::getBool(std::string valueName) {

    std::unordered_map<std::string, bool>::iterator it = dataBool.find(valueName);
    if (it == dataBool.end()) {

        SDL_Log("ERROR: Bool value %s does not exist", valueName.c_str());
        return false;

    }

    return it->second;

}

int Message::writeToFile(std::string fileName) {

    SDL_RWops *sdlFile = SDL_RWFromFile((fileName).c_str(), "w");
    //sdlFile->type = SDL_RWOPS_JNIFILE;
    if (!sdlFile) {
        SDL_Log("Cannot save file! : %s", SDL_GetError());
        return -1;
    }

    char delim = ':';
    char jump = '\n';
    char intVal = 'i';
    char boolVal = 'b';
    char longVal = 'l';
    char stringVal = 's';

    std::string valueStr;
    for (auto it = dataInt.begin(); it != dataInt.end(); ++it) {
        valueStr = StringTools::to_string(it->second);
        SDL_RWwrite(sdlFile, it->first.c_str(), sizeof(char), it->first.size());
        SDL_RWwrite(sdlFile, &delim, sizeof(char), 1);
        SDL_RWwrite(sdlFile, valueStr.c_str(), sizeof(char), valueStr.size());
        SDL_RWwrite(sdlFile, &delim, sizeof(char), 1);
        SDL_RWwrite(sdlFile, &intVal, sizeof(char), 1);
        SDL_RWwrite(sdlFile, &jump, sizeof(char), 1);
    }

    for (auto it = dataLong.begin(); it != dataLong.end(); ++it) {
        valueStr = StringTools::to_string(it->second);
        SDL_RWwrite(sdlFile, it->first.c_str(), sizeof(char), it->first.size());
        SDL_RWwrite(sdlFile, &delim, sizeof(char), 1);
        SDL_RWwrite(sdlFile, valueStr.c_str(), sizeof(char), valueStr.size());
        SDL_RWwrite(sdlFile, &delim, sizeof(char), 1);
        SDL_RWwrite(sdlFile, &longVal, sizeof(char), 1);
        SDL_RWwrite(sdlFile, &jump, sizeof(char), 1);
    }

    for (auto it = dataBool.begin(); it != dataBool.end(); ++it) {
        if (it->second == true) {
            valueStr = "1";
        }
        else {
            valueStr = "0";
        }
        SDL_RWwrite(sdlFile, it->first.c_str(), sizeof(char), it->first.size());
        SDL_RWwrite(sdlFile, &delim, sizeof(char), 1);
        SDL_RWwrite(sdlFile, valueStr.c_str(), sizeof(char), valueStr.size());
        SDL_RWwrite(sdlFile, &delim, sizeof(char), 1);
        SDL_RWwrite(sdlFile, &boolVal, sizeof(char), 1);
        SDL_RWwrite(sdlFile, &jump, sizeof(char), 1);

    }

    for (auto it = dataString.begin(); it != dataString.end(); ++it) {
        valueStr = it->second;
        SDL_RWwrite(sdlFile, it->first.c_str(), sizeof(char), it->first.size());
        SDL_RWwrite(sdlFile, &delim, sizeof(char), 1);
        SDL_RWwrite(sdlFile, &valueStr, sizeof(char), valueStr.size());
        SDL_RWwrite(sdlFile, &delim, sizeof(char), 1);
        SDL_RWwrite(sdlFile, &stringVal, sizeof(char), 1);
        SDL_RWwrite(sdlFile, &jump, sizeof(char), 1);
    }

    SDL_RWclose(sdlFile);
    return 0;

}

int Message::loadFromFile(std::string fileName, bool debug) {

    SDL_RWops *sdlFile = SDL_RWFromFile((fileName).c_str(), "r");

    if (!sdlFile) {
        if (debug)
            SDL_Log("Error opening file %s", fileName.c_str());
        return -1;

    }

    //sdlFile->type = SDL_RWOPS_JNIFILE;

    char data[SDL_RWsize(sdlFile)];
    SDL_RWread(sdlFile, data, sizeof data, 1);

    std::stringstream stream;

    stream << data;
    std::string line = "";

    char delim = ':';

    int j = 0;

    while (std::getline(stream, line)) {
        if (line.empty()) {
            continue;
        }

        std::string value = "";
        std::stringstream lineStream(line);
        j = 0;
        std::string data[3];
        int pos = 0;

        while (std::getline(lineStream, value, delim)) {
            data[pos] = value;
            ++pos;
        }

        if (data[2] == "i") {
            insertInt(data[0], StringTools::to_integer(data[1]));

        }
        else if (data[2] == "b") {
            bool b;
            if (data[1] == "1") {

                b = true;
            }
            else
                b = false;
            insertBool(data[0], b);

        }
        else if (data[2] == "s") {

            insertString(data[0], data[1]);

        }

        else if (data[2] == "l") {

            insertLong(data[0], StringTools::to_long(data[1]));

        }

    }

    SDL_RWclose(sdlFile);
    return 0;

}

void Message::merge(Message *other) {



}



std::vector<std::pair<std::string, std::string> > Message::getAllStrings() {

    std::vector<std::pair<std::string, std::string> > values;

    for (auto it = dataString.begin(); it != dataString.end(); ++it) {
        values.emplace_back(std::make_pair(it->first, it->second));
    }

    return values;

}

void Message::cleanUp() {

    dataString.clear();
    dataInt.clear();
    dataBool.clear();
    dataLong.clear();

}

