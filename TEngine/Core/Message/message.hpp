#ifndef MESSAGE_H
#define MESSAGE_H

#include <unordered_map>
#include <string>
#include "SDL.h"

#include <sstream>
#include <vector>
#include "Core/Tools/stringtools.hpp"


class Message {

    private:

        std::unordered_map<std::string, int>              dataInt;
        std::unordered_map<std::string, std::string>      dataString;
        std::unordered_map<std::string, bool>             dataBool;
        std::unordered_map<std::string, long>             dataLong;

    public:

        Message();
        void insertInt(std::string valueName, int value);
        void insertString(std::string valueName, std::string value);
        void insertBool(std::string valueName, bool value);
        void insertLong(std::string valueName, long value);

        // the method should return a int in case of fail!

        void retrieveInt(std::string valueName, int &fillValue);
        void retrieveLong(std::string valueName, long &fillValue);
        void retrieveBool(std::string valueName, bool *fillValue);
        int getInt(std::string valueName);
        std::string getString(std::string valueName);
        bool getBool(std::string valueName);

        int writeToFile(std::string fileName);
        int loadFromFile(std::string fileName, bool debug=true);

        void merge(Message *other);

        std::vector<std::pair<std::string, std::string> > getAllStrings();

        void cleanUp();

};

#endif // MESSAGE_H
