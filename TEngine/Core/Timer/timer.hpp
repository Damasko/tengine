#ifndef TIMER_H
#define TIMER_H

#include "SDL.h"

class Timer {

    public:

        static int FPS;
        static Uint64 NOW;
        static Uint64 LAST;
        static Uint64 ELAPSED;
        static float deltaTime;
        static int average[6];

    public:

    private:

        Timer();

};

#endif // TIMER_H
