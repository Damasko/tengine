#ifndef SCENE_HPP
#define SCENE_HPP

#include <vector>
#include "Core/GameObject/gameobject.hpp"
#include "Core/Message/message.hpp"

class Scene {

    private:

        //std::vector<GameObject*>    gameObjects;
        int                         gameObjectID;
        Message                     data;
        GameObject                  root;

    public:

        Scene();
        ~Scene();
        void init();
        void onStart();
        void update();
        void reset();
        void onExit();
        void cleanUp();

        void addGameObject(GameObject *go);
        std::vector<GameObject *> getGameObjects();
        GameObject *createGameObject();

};

#endif // SCENE_HPP
