#include "scene.hpp"


Scene::Scene() : gameObjectID(0) {

}

Scene::~Scene() {

}

void Scene::addGameObject(GameObject *go) {

    if (!go) {
        SDL_Log("Scene error: NULL gameobject");
        return;
    }

    root.addChild(go);

}

std::vector<GameObject*> Scene::getGameObjects() {

    return root.getChildren();

}

void Scene::cleanUp() {

    std::vector<GameObject*> objects = root.getChildren();

    for (auto it = objects.begin(); it != objects.end(); ++it) {
        (*it)->cleanUp();
        delete (*it);
    }

    root.getChildren().clear();

}

GameObject *Scene::createGameObject() {

    GameObject *created = new GameObject();
    created->setID(++gameObjectID);

    root.addChild(created);

    return created;

}

