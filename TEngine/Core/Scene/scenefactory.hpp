#ifndef SCENEFACTORY_HPP
#define SCENEFACTORY_HPP

#include <vector>
#include <string>
#include "Core/Scene/scene.hpp"

class SceneFactory {

    private:

        static std::vector<Scene*>     scenes;

    private:

        SceneFactory();

    public:

        static int loadSceneFromFile(std::string file);
        //static Scene* testScene();

};

#endif // SCENEFACTORY_HPP
