#include "inputsystem.hpp"

InputSystem::InputSystem() : hitbox(0, 0) {

    resetState();
    timerTap = 0.0f;

}

InputSystem::~InputSystem() {

}

int InputSystem::init() {

    SDL_ShowCursor(1);
    resetState();

    return 0;

}

void InputSystem::onStart() {

}

void InputSystem::update(std::vector<GameObject *> *list, float delta) {

    updateEventQueue();

    for (unsigned int i = 0; i < iComponents.size(); ++i) {

        if (iComponents[i]->getAction() == "FOLLOW_MOUSE") {

            Vector2 mousePos = Vector2(getPosX(), getPosY());

            //mousePos.x -= (Configuration::get().SCREEN_W / 2);
            //mousePos.y -= (Configuration::get().SCREEN_H / 2);
            //Vector2 objectPos = iComponents[i]->getOwner()->getTransform()->getPosition2D();
            //mousePos += objectPos;
            //mousePos *= Timer::deltaTime;
            iComponents[i]->getOwner()->getTransform()->setPosition(mousePos);

        }

        else if (iComponents[i]->getAction() == "ADD_MOUSE") {

            if (!inputs.press1Held)
                return;

            Vector2 mousePos = Vector2(getPosX(), getPosY());

            mousePos.x -= (Configuration::get().SCREEN_W / 2);
            mousePos.y -= (Configuration::get().SCREEN_H / 2);
            Vector2 objectPos = iComponents[i]->getOwner()->getTransform()->getPosition2D();
            mousePos *= Timer::deltaTime;
            objectPos += mousePos;
            iComponents[i]->getOwner()->getTransform()->setPosition(objectPos);

        }

        else if (iComponents[i]->getAction() == "CAMERA_FOLLOW") {

            Camera2D *camera = (Camera2D*) iComponents[i];
            /*
            Vector2 center = camera->getOwner()->getTransform()->getPosition2D();
            center.x -= camera->getRect()->w / 2;
            center.y -= camera->getRect()->h / 2;
            camera->setCenter(center);
            */
            //Vector2 distanceToOwner =
            camera->setCenter(camera->getOwner()->getTransform()->getPosition2D());

        }

    }

}

void InputSystem::onExit() {

}

void InputSystem::cleanUp() {



}

void InputSystem::updateEventQueue() {

    resetState();

    while (SDL_PollEvent(&event)) {

        updateEvent();

    }
    updateButtons();

}

void InputSystem::updateButtons() {

    #ifndef __ANDROID__
        //const Uint8 *keyState = SDL_GetKeyboardState(NULL);
        int x = 0, y = 0;
        Uint32 mouseState = SDL_GetMouseState(&x, &y);

        if (mouseState & SDL_BUTTON(SDL_BUTTON_LEFT)) {
            if (!inputs.press1Held) {
                inputs.press1 = true;
                inputs.press1Held = true;
            }
        }
        if (mouseState & SDL_BUTTON(SDL_BUTTON_RIGHT)) {
            inputs.press2 = true;
        }
        if (mouseState & SDL_BUTTON(SDL_BUTTON_MIDDLE)) {
            inputs.press3 = true;
        }

        hitbox.x = x;
        hitbox.y = y;

    #endif

}

void InputSystem::updateEvent() {

    #ifdef __ANDROID__
        int x = 0, y = 0;
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_AC_BACK) {
                inputs.exit = true;
            }

        }

        else if (event.type == SDL_FINGERUP) {
            inputs.press1Held = false;
            inputs.motion = false;
            if (timerTap <= 0.0f)
                timerTap = 0.3f;
        }

        else if (event.type == SDL_FINGERDOWN) {
            if (!inputs.press1Held) {
                inputs.press1 = true;
                inputs.press1Held = true;
                if (timerTap > 0.0f) {
                    inputs.doubleTap = true;
                    timerTap = 0.0f;
                }
            }
            x = event.tfinger.x * Configuration::get().SCREEN_W;
            y = event.tfinger.y * Configuration::get().SCREEN_H;
        }

        else if (event.type == SDL_FINGERMOTION) {
            x = event.tfinger.x * Configuration::get().SCREEN_W;
            y = event.tfinger.y * Configuration::get().SCREEN_H;
            inputs.motion = true;
        }

        if (x != 0 and y != 0) {
            hitbox.setDstX(x);
            hitbox.setDstY(y);
        }
        //hitbox.setCenter(x, y);

    #elif __LINUX__ // CAMBIAR PARA MACROS DE OTRAS PLATAFORMAS

        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                inputs.exit = true;
            }
        }

        else if (event.type == SDL_QUIT) {
            inputs.exitButton = true;
        }

        if (event.type == SDL_MOUSEBUTTONUP) {
            if (event.button.button == SDL_BUTTON_LEFT) {
                inputs.press1Held = false;
                if (timerTap <= 0.0f)
                    timerTap = 0.03f;
            }

        }

        else if (event.type == SDL_MOUSEBUTTONDOWN) {
            if (event.button.button == SDL_BUTTON_LEFT) {
                if (timerTap > 0.0f) {
                    inputs.doubleTap = true;
                    timerTap = 0.0f;
                }
            }

        }

        else if (event.type == SDL_MOUSEWHEEL) {

            mouseWheel += event.wheel.y;

        }

        else if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_p) {
                inputs.paused = !inputs.paused;
            }
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                inputs.exit = true;
            }

        }

        else if (event.type == SDL_MOUSEMOTION) {
            inputs.motion = true;

        }

    #endif
/*
    if (timerTap > 0.0f)
        timerTap -= Timer::deltaTime;
*/
}

void InputSystem::resetState() {

    inputs.press1       = false;
    inputs.motion       = false;
    inputs.press1Held   = false;
    inputs.press2       = false;
    inputs.press3       = false;
    inputs.press4       = false;
    inputs.doubleTap    = false;
    inputs.paused       = false;
    inputs.exit         = false;
    inputs.exitButton   = false;

    mouseWheel          = 0.0f;

}


double InputSystem::getPosX() {

    return hitbox.x;

}


double InputSystem::getPosY() {

    return hitbox.y;

}


Vector2 InputSystem::getHitboxPosition() {

    return hitbox;

}

void InputSystem::setSystemCursor(int state) {

    SDL_ShowCursor(state);

}

void InputSystem::registerComponent(Component *c) {

    if (!c) {
        SDL_Log("InputSystem Error: Component is NULL");
        return;
    }

    iComponents.emplace_back((InputComponent*) c);

}
