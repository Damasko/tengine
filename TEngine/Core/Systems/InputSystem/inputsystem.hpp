
#ifndef INPUTSYSTEM_HPP
#define INPUTSYSTEM_HPP

#include "SDL_gpu.h"
#include "Core/Systems/ISystem/system.hpp"
#include "Core/Math/vector2.hpp"

#include "Core/Component/InputComponent/inputcomponent.hpp"
#include "Core/GameObject/gameobject.hpp"
#include "Core/Config/configuration.hpp"
#include "Core/Timer/timer.hpp"

#include "Core/Component/Camera/camera2D.hpp"

struct Pressed {

    bool press1Held;
    bool motion;

    bool press1;
    bool press2;
    bool press3;
    bool press4;
    bool doubleTap;
    bool paused;
    bool exit;
    bool exitButton;

};


static const unsigned char NOT_PRESSED       = 0;
static const unsigned char PRESSED           = 1;

struct KeyAction {

    std::string actionName;
    unsigned char state = 0;
    unsigned char oldState = 0;

    bool isPressed() {

        return (state & PRESSED) == PRESSED;

    }

    bool isJustPressed() {

        return oldState == 0 and state == 1;

    }

    bool isReleased() {

        return oldState == 1 and state == 0;

    }

};

class InputSystem : public System {

    private:

        //std::unordered_map<
        std::vector<InputComponent*>    iComponents;
        Vector2     hitbox;

    public:

        Pressed     inputs;

        bool        held;
        bool        mHeld;
        SDL_Event   event;
        float       timerTap;
        float       mouseWheel;

    private:

        void updateButtons();
        void resetState();
        virtual void updateEventQueue();
        Vector2 getHitboxPosition();
        void setSystemCursor(int state);
        void updateEvent();
        void setKeyReading(bool state);
        double getPosX();
        double getPosY();

    public:

        InputSystem();
        ~InputSystem();
        int init();
        void onStart();
        void update(std::vector<GameObject* >*list, float delta);
        void onExit();
        void cleanUp();
        void registerComponent(Component *c);

};

#endif // INPUTSYSTEM_HPP
