#ifndef ISYSTEM_HPP
#define ISYSTEM_HPP

#include <vector>
#include "Core/GameObject/gameobject.hpp"

#include <algorithm>


inline bool sortByLayer(Component *a, Component *b) {

    return a->getOwner()->getLayer() < b->getOwner()->getLayer();

}

class System {

    public:

        virtual         ~System() { }
        virtual int     init()              = 0;
        virtual void    onStart()           = 0;
        virtual void    update(std::vector<GameObject*> *list, float delta) = 0;
        virtual void    onExit()            = 0;
        virtual void    cleanUp()           = 0;
        virtual void    registerComponent(Component *c) = 0;

};

#endif // ISYSTEM_HPP
