#ifndef RENDERINGSYSTEM_HPP
#define RENDERINGSYSTEM_HPP

#include "Core/Systems/ISystem/system.hpp"
#include "SDL_gpu.h"
#include "Core/Material/shader.hpp"
#include "Core/Config/configuration.hpp"

#include "Core/Component/GraphicComponent/sprite.hpp"
#include "Core/Component/GraphicComponent/tilemap.hpp"
#include "Core/Component/Camera/camera2D.hpp"
#include "Core/Component/GraphicComponent/shape.hpp"
#include "Core/Component/Transform/transform.hpp"
#include "Core/Tools/gametools.hpp"

struct RenderTarget {

    GPU_Target  *target = NULL;
    GPU_Image   *texture = NULL;
    Shader      *shader = NULL;
    SDL_Color   color   = { 0, 0, 0, 0 };

    int load(int w, int h, Shader *shad, GPU_FormatEnum type) {

        texture = GPU_CreateImage(w, h, type);

        if (!texture) {
            SDL_Log("Cannot create texture in render target");
            return -1;
        }

        target = GPU_LoadTarget(texture);

        if (!target) {
            SDL_Log("Cannot create the render target");
            return -2;

        }

        if (shad) {
            shader = shad;
        }

        return 0;

    }

    void copyTo(GPU_Target *other) {

        GPU_Blit(texture, NULL, other, Configuration::get().SCREEN_W / 2, Configuration::get().SCREEN_H / 2);

    }

    void copyTo(GPU_Target *other, GPU_Rect *dst) {

        GPU_BlitScale(texture, NULL, other, dst->x + (dst->w / 2), dst->y + (dst->h / 2), dst->w / texture->w, dst->h / texture->h);

    }

    void setShader(Shader *shad) {

        if (!shad) {
            SDL_Log("Shader is NULL");
            return;
        }

        shader = shad;

/*  Da problemas bindear la textura desde aqui, averiguar por que
        //shader->activate();
        GPU_SetShaderImage(texture, GPU_GetUniformLocation(shader->program, "tex"), 1);

*/
        shader->disable();
    }

    void clear() {
        GPU_ClearColor(target, color);
    }

    void cleanUp() {

        if (target != nullptr) {
            GPU_FreeTarget(target);
        }
        if (texture != nullptr) {
            GPU_FreeImage(texture);
        }
        if (shader != nullptr) {
            shader->cleanUp();
            delete(shader);
        }
    }
};


class RenderingSystem : public System {

    private:

        RenderTarget                defaultTarget;
        RenderTarget                *activeTarget;
        std::unordered_map<std::string, RenderTarget> renderTargets;
        std::vector<GraphicComponent*>  gComponents;
        std::vector<Camera2D*>          cameras;

        std::vector<Vector2>            lightPos;

        // BORRAR. TEST
        float                       timeDeleteMe;


    private:

        std::vector<GraphicComponent*> cullingObjects(Camera2D *cam);

    public:

        RenderingSystem();
        ~RenderingSystem();
        int init();
        void onStart();
        void update(std::vector<GameObject*> *list, float delta);
        void onExit();
        void cleanUp();
        void registerComponent(Component *c);

        void renderTexture(GPU_Image *image);
        void renderTexture(GPU_Image *image, float x, float y);
        void renderTexture(GPU_Image *image, GPU_Rect *dst);

        //void render(GraphicComponent *component);
        void render(Sprite *sprite);
        void render(Tilemap *tilemap);
        void render(Shape *shape);

        RenderTarget *createRenderTarget(std::string name, int w=Configuration::get().SCREEN_W, int h=Configuration::get().SCREEN_H, Shader *shader=NULL, GPU_FormatEnum type=GPU_FORMAT_RGBA);
        RenderTarget *getRenderTarget(std::string key);
        RenderTarget *getActiveTarget();
        int enableTarget(std::string key);
        int enableTarget(unsigned int layer);
        int setHasActiveTarget(RenderTarget *renderTarget);
        int deleteTarget(std::string key);
        int addRenderTarget(RenderTarget *rt);
        // Restores the current target to the the default target
        void restoreTarget(bool clear=false);
        void renderQueue(std::vector<std::string> renderTargetsKeys);
        RenderTarget *getDefaultRenderTarget();

        void addCamera(Camera2D *cam);

        // Test diffuse lighting
        void createDiffuseBuffers();
        void drawLightShadow(RenderTarget *occluder);

};

#endif // RENDERINGSYSTEM_HPP
