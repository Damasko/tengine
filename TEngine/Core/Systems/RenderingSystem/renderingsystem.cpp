#include "renderingsystem.hpp"



RenderingSystem::RenderingSystem() {

}

RenderingSystem::~RenderingSystem() {


}

int RenderingSystem::init() {

    defaultTarget.target = GPU_Init(Configuration::get().SCREEN_W, Configuration::get().SCREEN_H, GPU_DEFAULT_INIT_FLAGS);

    if (!defaultTarget.target) {
        SDL_Log("Error while creating window render target");
        return -1;
    }

    activeTarget = &defaultTarget;

    createDiffuseBuffers();

    timeDeleteMe = 0.0f;

    return 0;

}

void RenderingSystem::onStart() {

}

void RenderingSystem::update(std::vector<GameObject*> *list, float delta) {

    activeTarget->clear();

    RenderTarget *l1 = getRenderTarget("1");
    RenderTarget *l2 = getRenderTarget("2");
    l1->clear();
    l2->clear();

    /* *** COMMENT */
    //RenderTarget *l3 = getRenderTarget("3");
    RenderTarget *emissionTarget = getRenderTarget("emission");
    RenderTarget *diffuseTarget = getRenderTarget("diffuseTarget");
    RenderTarget *depthTarget = getRenderTarget("depth");
    RenderTarget *normalTarget = getRenderTarget("normalTarget");
    RenderTarget *lightsTarget = getRenderTarget("lightsTarget");
    RenderTarget *deferred = getRenderTarget("deferredTarget");
    RenderTarget *shadow = getRenderTarget("shadow");
    //emissionTarget->clear();
    diffuseTarget->clear();

    //enableTarget("diffuseTarget");
    setHasActiveTarget(diffuseTarget);
    /* *** COMMENT ***/

    if (cameras.size() > 0) {

        for (unsigned int i = 0; i < cameras.size(); ++i) {
            std::vector<GraphicComponent*> onCamera = cullingObjects(cameras[i]);
            /*
            for (unsigned int i = 0; i < onCamera.size(); ++i) {
                SDL_Log("%i", onCamera[i]->getOwner()->getLayer());
            }
            */
            std::sort(onCamera.begin(), onCamera.end(), sortByLayer);

            int layer = 1;
            if (onCamera.size() > 0) {
                layer = onCamera[0]->getOwner()->getLayer();
                enableTarget(layer);

            }
            for (unsigned int j = 0; j < onCamera.size(); ++j) {

                if (onCamera[j]->getOwner()->getLayer() != layer) {
                    layer = onCamera[j]->getOwner()->getLayer();
                    enableTarget(layer);
                }

                onCamera[j]->accept(this);

            }

        }

    }
    else {

        std::sort(gComponents.begin(), gComponents.end(), sortByLayer);
        for (unsigned int i = 0; i < gComponents.size(); ++i) {
            gComponents[i]->accept(this);
        }
    }


    /*
    //depth
    depthTarget->clear();
    depthTarget->shader->activate();
    int layLoc = GPU_GetUniformLocation(depthTarget->shader->program, "layer");
    GPU_SetUniformf(layLoc, 1.0);
    GPU_SetShaderImage(l1->texture, GPU_GetUniformLocation(depthTarget->shader->program, "tex"), 8);
    //GPU_SetColor(l1->texture, {50, 50, 50 , 255});
    l1->copyTo(depthTarget->target);
    //GPU_SetColor(l1->texture, {255, 255, 255 , 255});

    //GPU_SetColor(l2->texture, {10, 10, 10, 255});
    GPU_SetUniformf(layLoc, 0.7);
    GPU_SetShaderImage(l2->texture, GPU_GetUniformLocation(depthTarget->shader->program, "tex"), 9);
    l2->copyTo(depthTarget->target);
    //GPU_SetColor(l2->texture, {255, 255, 255 , 255});
    depthTarget->shader->disable();
    */
    /*
    emissionTarget->shader->activate();
    emissionTarget->copyTo(diffuseTarget->target);
    emissionTarget->shader->disable();
    */

    /*
    l1->copyTo(defaultTarget.target);
    l2->copyTo(defaultTarget.target);
    //l3->copyTo(diffuseTarget->target);
    */

    /* ***COMMENT */
    l1->copyTo(diffuseTarget->target);
    l2->copyTo(diffuseTarget->target);

    normalTarget->shader->activate();
    diffuseTarget->copyTo(normalTarget->target);
    normalTarget->shader->disable();

    //diffuseTarget->copyTo(defaultTarget.target);

    lightsTarget->clear();
    lightsTarget->shader->activate();

    int locCam = GPU_GetUniformLocation(lightsTarget->shader->program, "cameraPos");
    float updateCam[] = { (float) cameras[0]->getX(), (float) cameras[0]->getY()};
    GPU_SetUniformfv(locCam, 2, 1, updateCam);


    for (unsigned int i = 0; i < lightPos.size(); ++i) {
        lightPos[i].x += (50.0 * cos(timeDeleteMe) * Timer::deltaTime);
        int loc = GPU_GetUniformLocation(lightsTarget->shader->program, std::string("lightPos" + StringTools::to_string(i+1)).c_str());
        //float updateLight[] = { (float)(lightPos[i].x) - (float)cameras[0]->getX(), (float)lightPos[i].y - (float)cameras[0]->getY() };
        float updateLight[] = { (float)lightPos[i].x, (float)lightPos[i].y };
        GPU_SetUniformfv(loc, 2, 1, updateLight);
    }

    timeDeleteMe += Timer::deltaTime * 0.1;
    lightsTarget->copyTo(lightsTarget->target);
    lightsTarget->shader->disable();

    deferred->shader->activate();
    deferred->copyTo(defaultTarget.target);
    deferred->shader->disable();

    /***COMMENT */

    //drawLightShadow(l2);
    shadow->copyTo(defaultTarget.target);
    restoreTarget();

    GPU_Flip(activeTarget->target);

}

std::vector<GraphicComponent*> RenderingSystem::cullingObjects(Camera2D *cam) {

    // Simple AABB cambiar a Quadtree o BSP

    std::vector<GraphicComponent*> res;
    for (unsigned int i = 0; i < gComponents.size(); ++i) {

        if (GameTools::rectColliding(gComponents[i]->getDstRect(), cam->getRect())) {

            res.emplace_back(gComponents[i]);

        }

    }


    return res;

}

void RenderingSystem::renderTexture(GPU_Image *image) {

    GPU_BlitScale(image, NULL, activeTarget->target, Configuration::get().SCREEN_W / 2, Configuration::get().SCREEN_H / 2, Configuration::get().SCREEN_W / image->w, Configuration::get().SCREEN_H / image->h);

}

void RenderingSystem::renderTexture(GPU_Image *image, float x, float y) {

    GPU_Blit(image, NULL, activeTarget->target, x, y);

}

void RenderingSystem::renderTexture(GPU_Image *image, GPU_Rect *dst) {

    GPU_BlitScale(image, NULL, activeTarget->target, dst->x + (dst->w / 2), dst->y + (dst->h / 2), dst->w / image->w, dst->h / image->h);

}

/*
void RenderingSystem::render(GraphicComponent *component) {

    render(component);

}
*/

void RenderingSystem::render(Sprite *sprite) {

    Transform *tranform = sprite->getOwner()->getTransform();
    sprite->setCenter(tranform->getPosition2D());
    int val = 1;
    if (sprite->isFlipped())
        val = -1;
    if (cameras.size() > 0) {
        Camera2D *cam = cameras[0];
        GPU_BlitTransform(sprite->getImage(), sprite->getSrcRect(), activeTarget->target, sprite->getCenterX() - cam->getX(), sprite->getCenterY() - cam->getY(), tranform->getRotation2D(), val * sprite->getDstRect()->w / sprite->getSrcRect()->w, sprite->getDstRect()->h / sprite->getSrcRect()->h);
    }
    else {
        GPU_BlitTransform(sprite->getImage(), sprite->getSrcRect(), activeTarget->target, sprite->getCenterX(), sprite->getCenterY(), tranform->getRotation2D(), val * sprite->getDstRect()->w / sprite->getSrcRect()->w, sprite->getDstRect()->h / sprite->getSrcRect()->h);
    }

}

void RenderingSystem::render(Tilemap *tilemap) {

    GPU_Image *tileImage = tilemap->getTileset()->getImage();
    if (!tileImage) {
        SDL_Log("RenderingSystem Error: Tilemap has no image");
        return;
    }

    /* COMMENT
    RenderTarget *current = getActiveTarget();
    RenderTarget *emission = getRenderTarget("emission");
    COMMENT */

    GPU_Rect *tilemapArea = tilemap->getDstRect();
    GPU_Rect tile;
    tile.w = tilemap->getTileW();
    tile.h = tilemap->getTileH();
    tile.x = tilemapArea->x;
    tile.y = tilemapArea->y;
    float rotation = tilemap->getOwner()->getTransform()->getRotation2D();
    float scaleX = tilemap->getOwner()->getTransform()->getScale().x;
    float scaleY = tilemap->getOwner()->getTransform()->getScale().y;

    if (cameras.size() > 0) {

        Camera2D *cam = cameras[0];
        std::vector<Tile*> tilesToDraw = tilemap->getVisibleTiles(cam);

        for (unsigned int i = 0; i < tilesToDraw.size(); ++i) {

            if (tilesToDraw[i]->getProperty(IS_VISIBLE)) {

                GPU_BlitTransform(tileImage, &tilesToDraw[i]->src, activeTarget->target, (tilesToDraw[i]->position.x - cam->getX()) * scaleX, (tilesToDraw[i]->position.y - cam->getY()) * scaleY, rotation, scaleX, scaleY);

            }
            /*
            if (tilesToDraw[i]->getProperty(HAS_EMISSION)) {

                setHasActiveTarget(emission);
                GPU_BlitTransform(tileImage, &tilesToDraw[i]->src, activeTarget->target, (tilesToDraw[i]->position.x - cam->getX()) * scaleX, (tilesToDraw[i]->position.y - cam->getY()) * scaleY, rotation, scaleX, scaleY);
                setHasActiveTarget(current);

            }
            */

        }

    }
    else {

        std::vector<Tile> *tilesToDraw = tilemap->getTiles();
        for (unsigned int i = 0; i < tilesToDraw->size(); ++i) {
            if (tilesToDraw->at(i).getProperty(IS_VISIBLE)) {
                tile.x = tilemapArea->x + (i % tilemap->getMapW() * tile.w * scaleX);
                tile.y = tilemapArea->y + (i / tilemap->getMapW() * tile.h * scaleY);
                GPU_BlitTransform(tileImage, &tilesToDraw->at(i).src, activeTarget->target, tile.x, tile.y, rotation, scaleX, scaleY);
            }

        }
    }

    /*
    RenderTarget *lightTarget = getRenderTarget("1");
    Shader *lightsShader = lightTarget->shader;
    for (unsigned int i = 0; i < lightPos.size(); ++i) {
        int loc = GPU_GetUniformLocation(lightsShader->program, std::string("lightPos" + StringTools::to_string(i)).c_str());
        float lPos[] = {lightPos[i].x -  cameras[0]->getX() , lightPos[i].y - cameras[0]->getY()};
        GPU_SetUniformfv(loc, 2, 1, lPos);

    }
    */


}

void RenderingSystem::render(Shape *shape) {

    if (!shape)
        return;

    switch (shape->getType()) {
    case ShapeType::RECTANGLE:
        std::vector<float> *values = shape->getValues();
        //GPU_Rectangle(activeTarget->target, shape->);
        break;

    //case ShapeType::CIRCLE:

      //  break;

    //default:
      //  break;
    }


    //LASTTT

}

RenderTarget *RenderingSystem::createRenderTarget(std::string name, int w, int h, Shader *shader, GPU_FormatEnum type) {

    // Deberia reservar memoria dinamicamente?, usar smartpointer? revisar. Actualmente en crea una copia
    // del renderer target al introducirlo en el vector.
    RenderTarget rt;
    if (rt.load(w, h, shader, type) < 0) {

        return NULL;

    }

    auto it = renderTargets.find(name);

    if (it == renderTargets.end()) {
        renderTargets.insert(std::make_pair(name, rt));

    }
    else {
        it->second.cleanUp();
        it->second = rt;

    }

    // Cambiar definitivamente, permite ahorrar esta busqueda doble.
    it = renderTargets.find(name);
    return &it->second;

}

RenderTarget *RenderingSystem::getRenderTarget(std::string key) {

    auto it = renderTargets.find(key);

    if (it == renderTargets.end()) {

        SDL_Log("Render target with key \"%s\" not found", key.c_str());
        return NULL;

    }

    return &it->second;

}

RenderTarget *RenderingSystem::getActiveTarget() {

    return activeTarget;

}

int RenderingSystem::enableTarget(std::string key) {

    auto it = renderTargets.find(key);

    if (it == renderTargets.end()) {

        SDL_Log("Render target with key \"%s\" not found", key.c_str());
        return -1;

    }

    activeTarget = &it->second;
    return 0;

}

int RenderingSystem::enableTarget(unsigned int layer) {

    if (enableTarget(StringTools::to_string(layer)) < 0) {
        return -1;
    }

    return 0;

}

// FUNCION PELIGROSA, USAR CON PRECAUCION
int RenderingSystem::setHasActiveTarget(RenderTarget *renderTarget) {

    if (!renderTarget) {
        SDL_Log("RenderSystem Error: NULL RenderTarget");
        return -1;
    }

    activeTarget = renderTarget;

    return 0;

}

int RenderingSystem::deleteTarget(std::string key) {

    if (key == "default") {

        SDL_Log("ERROR: Attempt to delete default target");
        return -1;

    }

    auto it = renderTargets.find(key);

    if (it == renderTargets.end()) {
        SDL_Log("Cannot delete target. Not found");
        return -1;
    }

    it->second.cleanUp();
    renderTargets.erase(it);
    return 0;
}

void RenderingSystem::restoreTarget(bool clear) {

    if (clear)
        defaultTarget.clear();
    activeTarget = &defaultTarget;

}

void RenderingSystem::renderQueue(std::vector<std::string> renderTargetsKeys) {

    //std::vector<RenderTarget> rt

}

RenderTarget *RenderingSystem::getDefaultRenderTarget() {

    return &defaultTarget;

}

void RenderingSystem::onExit() {

}

void RenderingSystem::cleanUp() {

    for (auto it = renderTargets.begin(); it != renderTargets.end(); ++it) {

        it->second.cleanUp();

    }

    gComponents.clear();

}

void RenderingSystem::registerComponent(Component *c) {

    if (!c) {
        SDL_Log("RenderingSystem Error: Component is NULL");
        return;
    }

    gComponents.emplace_back((GraphicComponent*) c);

}

void RenderingSystem::addCamera(Camera2D *cam) {

    cameras.emplace_back(cam);

}

void RenderingSystem::createDiffuseBuffers() {

    lightPos.emplace_back(Vector2(200, 300));
    lightPos.emplace_back(Vector2(500, 600));
    lightPos.emplace_back(Vector2(800, 400));
    lightPos.emplace_back(Vector2(100, 470));
    lightPos.emplace_back(Vector2(400, 870));


    RenderTarget *l1 = createRenderTarget("1");
    RenderTarget *l2 = createRenderTarget("2");
    RenderTarget *l3 = createRenderTarget("3");

    RenderTarget *diffuseTarget = createRenderTarget("diffuseTarget");
    RenderTarget *normalTarget = createRenderTarget("normalTarget");
    RenderTarget *lightsTarget = createRenderTarget("lightsTarget");
    RenderTarget *deferredTarget = createRenderTarget("deferredTarget");
    RenderTarget *emissionTarget = createRenderTarget("emission");
    RenderTarget *depthTarget = createRenderTarget("depth");
    //RenderTarget *shadowLightTarget = createRenderTarget("shadow_light", 256, 1);
    RenderTarget *shadowTarget = createRenderTarget("shadow");

    Shader *normalShader = new Shader();
    normalShader->loadBlock("normals");

    normalTarget->setShader(normalShader);

    normalShader->activate();
    int screenLoc = GPU_GetUniformLocation(normalShader->program, "screen");
    float screen[] = { (float) Configuration::get().SCREEN_W, (float) Configuration::get().SCREEN_H };
    GPU_SetUniformfv(screenLoc, 2, 1, screen);
    GPU_SetShaderImage(defaultTarget.texture, GPU_GetUniformLocation(normalShader->program, "tex"), 1);
    normalShader->disable();

    Shader *lightsShader = new Shader();
    lightsShader->loadBlock("lights2d");

    lightsTarget->setShader(lightsShader);
    lightsShader->activate();
    screenLoc = GPU_GetUniformLocation(lightsShader->program, "screen");
    GPU_SetUniformfv(screenLoc, 2, 1, screen);
    GPU_SetShaderImage(lightsTarget->texture, GPU_GetUniformLocation(lightsShader->program, "tex"), 1);

    for (unsigned int i = 0; i < lightPos.size(); ++i) {
        int loc = GPU_GetUniformLocation(lightsShader->program, std::string("lightPos" + StringTools::to_string(i)).c_str());
        float lPos[] = { lightPos[i].x , lightPos[i].y };
        GPU_SetUniformfv(loc, 2, 1, lPos);

    }

    lightsShader->disable();

    Shader *deferred = new Shader();
    deferred->loadBlock("deferred2d");

    deferredTarget->setShader(deferred);
    deferred->activate();
    screenLoc = GPU_GetUniformLocation(deferred->program, "screen");
    GPU_SetUniformfv(screenLoc, 2, 1, screen);
    GPU_SetShaderImage(diffuseTarget->texture, GPU_GetUniformLocation(deferred->program, "diffuseTex"), 1);
    GPU_SetShaderImage(normalTarget->texture, GPU_GetUniformLocation(deferred->program, "normalsTex"), 2);
    GPU_SetShaderImage(lightsTarget->texture, GPU_GetUniformLocation(deferred->program, "lightsTex"), 3);
    GPU_SetShaderImage(emissionTarget->texture, GPU_GetUniformLocation(deferred->program, "emissionTex"), 4);
    GPU_SetShaderImage(depthTarget->texture, GPU_GetUniformLocation(deferred->program, "depthTex"), 4);
    deferred->disable();


    Shader *emissionShader = new Shader();
    emissionShader->loadBlock("emission");
    emissionTarget->setShader(emissionShader);
    emissionShader->activate();
    screenLoc = GPU_GetUniformLocation(emissionShader->program, "screen");
    GPU_SetUniformfv(screenLoc, 2, 1, screen);
    GPU_SetShaderImage(emissionTarget->texture, GPU_GetUniformLocation(emissionShader->program, "tex"), 5);
    emissionShader->disable();


    Shader *depthShader = new Shader();
    depthShader->loadBlock("depth");
    depthTarget->setShader(depthShader);
    depthShader->activate();
    screenLoc = GPU_GetUniformLocation(emissionShader->program, "screen");
    GPU_SetUniformfv(screenLoc, 2, 1, screen);
    GPU_SetShaderImage(depthTarget->texture, GPU_GetUniformLocation(depthShader->program, "tex"), 5);
    depthShader->disable();

    Shader *shadowShader = new Shader();
    shadowShader->loadBlock("shadow");
    shadowTarget->setShader(shadowShader);
    shadowShader->activate();
    screenLoc = GPU_GetUniformLocation(shadowShader->program, "screen");
    GPU_SetUniformfv(screenLoc, 2, 1, screen);
    GPU_SetShaderImage(l2->texture, GPU_GetUniformLocation(shadowShader->program, "tex"), 6);
    GPU_SetShaderImage(lightsTarget->texture, GPU_GetUniformLocation(shadowShader->program, "lightTex"), 7);
    depthShader->disable();

    timeDeleteMe = 0.0f;

}

void RenderingSystem::drawLightShadow(RenderTarget *occluder) {

    RenderTarget *shadow = getRenderTarget("shadow");
    //RenderTarget *shadowLight = getRenderTarget("shadow_light");
    shadow->color = {255, 255, 255, 255};
    shadow->clear();
    GPU_Rect lightRect;
    lightRect.w = lightRect.h = 512;
    lightRect.x = lightRect.y = 0;
    //shadow->shader->activate();
    GPU_SetColor(occluder->texture, {0, 0, 0, 255});
    for (unsigned int i = 0; i < lightPos.size(); ++i) {

        /*
        lightRect.x = (lightPos[i].x - lightRect.w / 2 - cameras[0]->getX());
        lightRect.y = (lightPos[i].y - lightRect.h / 2 - cameras[0]->getY());
        //GPU_SetClip(occluder->target, lightRect.x, lightRect.y, lightRect.w, lightRect.h);
        //GPU_Blit(occluder->texture, &lightRect, shadow->target, lightRect.x - cameras[0]->getX(), lightRect.y - cameras[0]->getY());
        GPU_Blit(occluder->texture, &lightRect, shadow->target, lightRect.x - cameras[0]->getX(), lightRect.y - cameras[0]->getY());
        */
        lightRect.x = (lightPos[i].x - lightRect.w / 2 - cameras[0]->getX());
        lightRect.y = (lightPos[i].y - lightRect.h / 2 - cameras[0]->getY());
        GPU_Blit(occluder->texture, &lightRect, shadow->target, lightRect.x + lightRect.w / 2, lightRect.y + lightRect.h / 2);

    }
    //shadow->shader->disable();
    //GPU_UnsetClip(occluder->target);
    GPU_SetColor(occluder->texture, {255, 255, 255, 255});

}
