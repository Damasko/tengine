#include "vector2.hpp"

Vector2::Vector2() : x(0.0f), y(0.0f) {


}

Vector2::Vector2(float nx, float ny) : x(nx), y(ny) {


}

float Vector2::distance() {

    return sqrt(pow(x, 2.0f) + pow(y, 2.0f));

}

// Optimized version, without sqrt
float Vector2::squaredDistance() {

    return (pow(x, 2.0f) + pow(y, 2.0f));

}

void Vector2::normalize() {

    float dist = distance();
    x /= dist;
    y /= dist;

}
