#ifndef VECTOR2_H
#define VECTOR2_H

#include <cmath>
#include "SDL.h"

class Vector2 {

    public:

        double       x;
        double       y;

    public:

        Vector2();
        Vector2(float nx, float ny);
        float distance();
        float squaredDistance();
        void normalize();


        void operator+(const Vector2 &other) {

            this->x += other.x;
            this->y += other.y;

        }


        void operator *(const Vector2 &other) {

            this->x *= other.x;
            this->y *= other.y;

        }

        void operator *(const double &scalar) {

            this->x *= scalar;
            this->y *= scalar;

        }

        void operator *=(const double &scalar) {

            this->x *= scalar;
            this->y *= scalar;

        }

        void operator +(const double scalar) {

            this->x += scalar;
            this->y += scalar;

        }

        void operator +=(const double &scalar) {
            this->x += scalar;
            this->y += scalar;

        }

        void operator +(Vector2 &other) {
            this->x += other.x;
            this->y += other.y;
        }

        void operator +=(Vector2 &other) {
            this->x += other.x;
            this->y += other.y;

        }


        Vector2 operator -(const Vector2 &other) {
            Vector2 res;
            res.x = this->x - other.x;
            res.y = this->y - other.y;
            return res;
        }

        static inline Vector2 rotate(Vector2 target, double rotation) {
            Vector2 res;

            rotation *= 3.141592 / 180.0;

            double sinRot = sin(rotation);
            double cosRot = cos(rotation);

            res.x = target.x * cosRot - target.y * sinRot;
            res.y = target.x * sinRot + target.y * cosRot;

            return res;

        }

        static inline Vector2 rotate(Vector2 target, Vector2 origin, double rotation, double scalar=1.0) {

            Vector2 res = rotate(target, rotation);

            res.x = (res.x * scalar) + origin.x;
            res.y = (res.y * scalar) + origin.y;
            return res;

        }

};

#endif // VECTOR2_H
