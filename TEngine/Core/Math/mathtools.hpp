#ifndef MATHTOOLS_HPP
#define MATHTOOLS_HPP

#include "Core/Math/vector2.hpp"
#include "Core/Math/vector3.hpp"
#include "SDL_gpu.h"

namespace MathTools {

    static const double     PI      = 3.141592;
    static const double     DEGREES = (180.0 / PI);
    static const double     RADIANS = (PI / 180.0);

    static inline bool rectColliding(const GPU_Rect *a, const GPU_Rect *b) {

        if( (a->y + a->h) < b->y)
            return false;

        if( a->y > (b->y + b->h))
            return false;

        if( (a->x + a->w) < b->x)
            return false;

        if( a->x > (b->x + b->w))
            return false;

        return true;

    }

    /*
    inline bool circleEdgeColliding(Vector2 edge, Vector2 circlePosition, float radius) {
        // TODO
        return true;

    }
    */

    static inline bool rectPointColliding(const GPU_Rect *rect, const Vector2 point) {

        if (point.x < rect->x or point.x > rect->x + rect->w) {
            return false;
        }
        if (point.y < rect->y or point.y > rect->y + rect->h) {
            return false;
        }

        return true;

    }

    static inline bool circlesColliding(Vector2 center1, Vector2 center2, float radius1, float radius2) {

        float length = radius1 + radius2;
        //squared
        length *= length;

        Vector2 circlesVector = center2 - center1;

        if (circlesVector.squaredDistance() <= length) {
            return true;
        }

        return false;
    }

    template<class T>
    static inline void clamp( T& v, const T& lo, const T& hi ) {
        if (v < lo) v = lo;
        else if (v > hi) v = hi;
    }

}

#endif // MATHTOOLS_HPP
