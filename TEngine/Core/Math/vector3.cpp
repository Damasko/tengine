#include "vector3.hpp"

Vector3::Vector3() : x(0.0), y(0.0), z(0.0) {


}

Vector3::Vector3(double nx, double ny, double nz) : x(nx), y(ny), z(nz) {

}

float Vector3::distance() {

    return sqrt(pow(x, 2.0) + pow(y, 2.0) + pow(z, 2.0));

}

// Optimized version, without sqrt
float Vector3::fakeDistance() {

    return (pow(x, 2.0) + pow(y, 2.0) + pow(z, 2.0));

}

void Vector3::normalize() {

    float dist = distance();
    x /= dist;
    y /= dist;
    z /= dist;

}
