#ifndef Vector3_H
#define Vector3_H

#include <cmath>
#include "SDL.h"

class Vector3 {

    public:

        double       x;
        double       y;
        double       z;

    public:

        Vector3();
        Vector3(double nx, double ny, double nz);
        float distance();
        float fakeDistance();
        void normalize();


        void operator+(const Vector3 &other) {

            this->x += other.x;
            this->y += other.y;
            this->z += other.z;

        }


        void operator *(const Vector3 &other) {

            this->x *= other.x;
            this->y *= other.y;
            this->z *= other.z;

        }

        void operator *(const double &scalar) {

            this->x *= scalar;
            this->y *= scalar;
            this->z *= scalar;

        }

        void operator *=(const double &scalar) {

            this->x *= scalar;
            this->y *= scalar;
            this->z *= scalar;

        }

        void operator +(const double scalar) {

            this->x += scalar;
            this->y += scalar;
            this->z += scalar;

        }

        void operator +=(const double &scalar) {
            this->x += scalar;
            this->y += scalar;
            this->z += scalar;

        }

        void operator +(Vector3 &other) {
            this->x += other.x;
            this->y += other.y;
            this->z += other.z;
        }

        void operator +=(Vector3 &other) {

            this->x += other.x;
            this->y += other.y;
            this->z += other.z;

        }


        Vector3 operator -(const Vector3 &other) {
            Vector3 res;
            res.x = this->x - other.x;
            res.y = this->y - other.y;
            res.z = this->z - other.z;
            return res;
        }

        static inline Vector3 rotate(Vector3 target, double rotation) {
            Vector3 res;

            rotation *= 3.141592 / 180.0;

            double sinRot = sin(rotation);
            double cosRot = cos(rotation);

            res.x = target.x * cosRot - target.y * sinRot;
            res.y = target.x * sinRot + target.y * cosRot;

            return res;

        }

        static inline Vector3 rotate(Vector3 target, Vector3 origin, double rotation, double scalar=1.0) {

            Vector3 res = rotate(target, rotation);

            res.x = (res.x * scalar) + origin.x;
            res.y = (res.y * scalar) + origin.y;
            return res;

        }

};

#endif // Vector3_H
