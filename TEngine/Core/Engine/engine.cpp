 #include "engine.hpp"

Engine::Engine() : state(UNINITIALIZED), renderSystem(nullptr), inputSystem(nullptr), activeScene(nullptr) {

}

int Engine::init() {

    inputSystem = new InputSystem();

    if (multimedia.init() < 0) {
        SDL_Log("Error in multimedia");
        return -1;

    }

    if (inputSystem->init() < 0) {
        delete inputSystem;
        cleanUp();
        return -2;
    }

    addSystem(inputSystem);

    renderSystem = new RenderingSystem();

    if (renderSystem->init() < 0) {

        delete renderSystem;
        cleanUp();
        return -1;

    }

    addSystem(renderSystem);

    testScene();

    state = INITIALIZED;

    return 0;

}

void Engine::addSystem(System *system) {

    if (system) {

        systems.emplace_back(system);

    }

}

void Engine::run() {

    state = RUNNING;

    while (state != EXIT) {

        std::vector<GameObject*> gObjects = activeScene->getGameObjects();

        for (unsigned int i = 0; i < systems.size(); ++i) {

            systems[i]->update(&gObjects, Timer::deltaTime);

        }


        checkCondition();
        SDL_Delay(5);

    }

    cleanUp();

}

void Engine::checkCondition() {

    if (inputSystem->inputs.exit) {

        state = EXIT;

    }

}

void Engine::addScene(Scene *scene, bool setHasActive) {

    if (!scene) {
        SDL_Log("Error: NULL scene");
        return;
    }

    scenes.emplace_back(scene);
    if (setHasActive)
        activeScene = scene;

}

void Engine::popScene() {

    if (scenes.size() > 1) {
        Scene *s = scenes.back();
        s->cleanUp();
        scenes.pop_back();
        delete s;
    }

}

void Engine::testScene() {

    multimedia.images.loadImage("rejilla.png", "rejilla");
    //multimedia.images.loadImage("rejilla_n.png", "rejilla_n");

    multimedia.images.loadImage("tileset1.png", "tile_back");
    multimedia.images.loadImage("tileset2.png", "tile_front");

    multimedia.images.loadImage("pistol.png", "pistol");
    multimedia.images.loadImage("robot.png", "robot");

    Scene *test = new Scene();


    GameObject *tileObject1 = test->createGameObject();
    tileObject1->setName("Tilemap 1");

    tileObject1->getTransform()->setPosition(Vector2(Configuration::get().SCREEN_W / 2, Configuration::get().SCREEN_H / 2));
    tileObject1->setLayer(1);

    Tilemap *tilemap = new Tilemap(tileObject1);

    if (tilemap->loadTileset(multimedia.images.getImage("tile_back"), "tileset1") < 0) {
        return;
    }

    if (tilemap->loadFromFile("mapa0") < 0) {
        return;
    }

    registerComponent(tilemap);

    tileObject1->addComponent(tilemap);

    GameObject *tileObject2 = test->createGameObject();
    tileObject2->setName("Tilemap 2");
    tileObject2->setLayer(2);
    Tilemap *tilemap2 = new Tilemap(tileObject2);

    if (tilemap2->loadTileset(multimedia.images.getImage("tile_front"), "tileset2") < 0) {
        return;
    }

    if (tilemap2->loadFromFile("mapa1") < 0) {
        return;
    }

    tilemap2->setPosition(Vector2(0, 0));

   registerComponent(tilemap2);

    tileObject2->addComponent(tilemap2);


    // Pistola
    GameObject *pistol = test->createGameObject();
    pistol->setName("Pistol");
    pistol->getTransform()->setPosition(Vector2(128, 128));
    pistol->setLayer(2);

    Sprite *pistolSprite = new Sprite(pistol);
    pistolSprite->setImage(multimedia.images.getImage("pistol"));
    pistolSprite->setSrc(NULL);
    pistolSprite->setDst(0, 0, 40, 26);

    registerComponent(pistolSprite);

    InputComponent *followMouse = new InputComponent(pistol);
    followMouse->setAction("ADD_MOUSE");

    registerComponent(followMouse);

    Camera2D *camera = new Camera2D(pistol);
    pistol->setName("Pistol");

    registerComponent(camera);
    renderSystem->addCamera(camera);

    addScene(test);

}

void Engine::registerComponent(Component *component) {

    if (Component::hasToRegisterInSystem(component, SYS_RENDERING)) {
        renderSystem->registerComponent(component);
    }

    if (Component::hasToRegisterInSystem(component, SYS_INPUT)) {
        inputSystem->registerComponent(component);
    }

}

void Engine::cleanUp() {

    multimedia.cleanUp();
    activeScene = NULL;

    for (unsigned int i = 0; i < systems.size(); ++i) {

        systems[i]->cleanUp();
        delete systems[i];
        systems[i] = NULL;

    }

    for (unsigned int i = 0; i < scenes.size(); ++i) {

        scenes[i]->cleanUp();
        delete scenes[i];
        scenes[i] = NULL;

    }

    GPU_Quit();

    state = EXIT;

}
