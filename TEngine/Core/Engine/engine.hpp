#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <vector>

#include "Core/Timer/timer.hpp"
#include "Core/Scene/scenefactory.hpp"
#include "Core/GameObject/gameobjectfactory.hpp"

#include "Core/Multimedia/multimedia.hpp"

#include "Core/Systems/InputSystem/inputsystem.hpp"
#include "Core/Systems/RenderingSystem/renderingsystem.hpp"

enum EngineState {

    UNINITIALIZED,
    INITIALIZED,
    RUNNING,
    PAUSED,
    EXIT

};

class Engine {

    private:

        EngineState             state;

        Multimedia              multimedia;

        RenderingSystem         *renderSystem;
        InputSystem             *inputSystem;

        std::vector<System*>    systems;
        std::vector<Scene*>     scenes;
        Scene                   *activeScene;

    private:

        void addSystem(System *system);
        virtual void checkCondition();
        void addScene(Scene *scene, bool setHasActive=true);
        void popScene();
        void testScene();
        void cleanUp();

        // Mover de engine. Funcion que coge un componente y lo registra en el sistema adecuado
        void registerComponent(Component *component);

    public:

        Engine();
        int init();
        void run();
        void update();

};

#endif // ENGINE_HPP
