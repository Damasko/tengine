
#include "Core/Engine/engine.hpp"

int main() {

    Engine engine;

    if (engine.init() < 0)
        return -1;

    engine.run();

    return 0;
}
