#ifdef GL_ES
precision highp float;
#endif

varying vec4 color;
varying vec2 texCoord;

uniform sampler2D tex;
uniform float layer;

uniform vec2 screen;

void main() {

    vec4 color = texture2D(tex, texCoord);
    //float blackWhite = (color.r + color.g + color.b) / 3.0;

    //gl_FragColor = layer * vec4(blackWhite, blackWhite, blackWhite, color.a);
    gl_FragColor = vec4(layer, layer, layer, color.a);

}
