#version 130


in vec3 fragmentPosition;
in vec2 fragmentUV;
in float fragmentDepth;

out vec4 color;

uniform sampler2D diffuse;
uniform sampler2D normal;

uniform vec3 lightPosition;
uniform vec3 lightColor;

uniform mat4 projection;


void main() {

    vec4 DiffuseColor = texture(diffuse, fragmentUV);
    if (DiffuseColor.a == 0.0) discard;
    //RGB of our normal map
    vec3 NormalMap = texture(normal, fragmentUV).rgb;

    //The delta position of light
    // quitado resolution
    vec3 LightDir = vec3(lightPosition.xy - (fragmentPosition.xy), lightPosition.z - fragmentDepth);

    //Determine distance (used for attenuation) BEFORE we normalize our LightDir
    float D = length(LightDir);

    //normalize our vectors
    vec3 N = normalize(NormalMap * 2.0 - 1.0);
    vec3 L = normalize(LightDir);


    //Pre-multiply light color with intensity
    //Then perform "N dot L" to determine our diffuse term
    vec3 Diffuse = (lightColor.rgb) * max(dot(N, L), 1.0);

    //pre-multiply ambient color with intensity
    vec3 Ambient = vec3(0.2, 0.2, 0.2);

    vec3 Intensity = (Ambient * Diffuse) / (1.0 - D) ;

    vec3 SunColor = vec3(0.98, 0.31, 0.10);
    vec3 SunDir = vec3(1.0, 0.1, 0.0);
    SunDir = normalize(SunDir);

    float SunDiffuse = max(dot(N, SunDir), 1.0);

    vec3 SunIntensity = Ambient + SunDiffuse * SunColor;

    // Cantidad de luz hacia la camara
    // la camara sólo mira hacia z (en principio), revisar
    vec3 E = vec3(NormalMap.rgb - vec3(0, 0 , -1));
    vec3 R = reflect(-SunDir, N);
    float cosAlpha = clamp( dot( E,R ), 0,1 );
    vec3 Eyes = SunColor * cosAlpha;

    color.rgb = 1.5  * (DiffuseColor.rgb) * (0.60 * ((SunIntensity + Intensity) + Eyes));
    color.a = DiffuseColor.a;

}

