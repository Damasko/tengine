#version 130

in vec3 vertexPosition;
in vec2 vertexUV;
in float vertexDepth;

out vec3 fragmentPosition;
//out vec4 fragmentColor;
out vec2 fragmentUV;
out float fragmentDepth;

uniform mat4 projection;

void main() {
    //Set the x,y position on the screen
    gl_Position.xyz = vec3(projection * vec4(vertexPosition.x, vertexPosition.y, vertexDepth, 1.0)).xyz;
    //gl_Position.xyz = vertexPosition.xyz;
    //the z position is zero since we are in 2D
    //gl_Position.z = 0.0;

    //Indicate that the coordinates are normalized
    gl_Position.w = 1.0;

    fragmentPosition = vertexPosition;

    //fragmentColor = vertexColor;

    fragmentUV = vec2(vertexUV.x, 1.0 - vertexUV.y);
    fragmentDepth = vertexDepth;
}
