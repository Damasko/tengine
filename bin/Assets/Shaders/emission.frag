#ifdef GL_ES
precision highp float;
#endif

varying vec4 color;
varying vec2 texCoord;

uniform sampler2D tex;

uniform vec2 screen;

/*
const int steps = 8;


void main(void) {

    vec4 sum = vec4(0.0);

    int j;
    int i;

    for( i= -steps ;i < steps; ++i) {
        for (j = -steps; j < steps; ++j) {

            sum += texture2D(tex, texCoord + vec2(j, i) * 0.003) * 0.03;

        }
    }

    float grey = (sum.r + sum.g, sum.b) / 3.0;

    sum = vec4(vec3(grey), sum.a);

    gl_FragColor = sum;

}


*/



const int blur_size = 1;
const float blur_width = 2.;


float gauss(float x, float e) {
    return exp(-pow(x, 2.)/e);
}

vec4 horizontalBlur() {

    vec2 pos = texCoord;
    vec4 pixval = vec4(0.);
    float tot = 0.;

    const int nb = 2*blur_size+1;

    for (int x=0; x<nb; x++)    {
       float x2 = blur_width*float(x-blur_size);
       vec2 ipos = pos + vec2(x2/ 2.0 * screen.x, 0.);
       float g = gauss(x2, float(20*blur_size));
       pixval+= g * texture2D(tex, ipos);
       tot+= g;
    }

    return pixval/tot;

}

vec4 verticalBlur() {

    vec2 pos = texCoord;
    vec4 pixval = vec4(0.);
    float tot = 0.;

    const int nb = 2*blur_size+1;

    for (int x=0; x<nb; x++) {
        float x2 = blur_width*float(x-blur_size);
        vec2 ipos = pos + vec2(0., x2/screen.y);
        float g = gauss(x2, float(20*blur_size));
        pixval+= g*texture2D(tex, ipos);
        tot+= g;
    }
    return pixval/tot;

}

vec4 dithering( vec2 vScreenPos ){
    vec3 vDither = vec3( dot( vec2( 171.0, 231.0 ), vScreenPos.xy ) );
    vDither.rgb = fract( vDither.rgb / vec3( 103.0, 71.0, 97.0 ) );
    return vec4(vDither.rgb / 255.0,1.0);
}


void main() {

    vec4 blackWhite = verticalBlur() * horizontalBlur();

    float grey = (blackWhite.r + blackWhite.g, blackWhite.b) / 3.0;

    blackWhite = vec4(vec3(grey), blackWhite.a);
    //blackWhite += dithering(gl_FragCoord.xy);

    //vec4 blackWhite = texture2D(tex, texCoord);
    //float greyScale = (blackWhite.r + blackWhite.g + blackWhite.b) / 3.0;
    //blackWhite = vec4(greyScale, greyScale, greyScale, 1.0)

    gl_FragColor = blackWhite * 3.0;

}

