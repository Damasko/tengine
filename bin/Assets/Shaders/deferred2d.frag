#ifdef GL_ES
precision highp float;
#endif

varying vec4 color;
varying vec2 texCoord;

uniform sampler2D diffuseTex;
uniform sampler2D normalsTex;
uniform sampler2D lightsTex;
uniform sampler2D emissionTex;
uniform sampler2D depthTex;

uniform vec2 screen;


void main() {

    vec4 diffuse = texture2D(diffuseTex, texCoord);
    vec4 normals = texture2D(normalsTex, texCoord);
    vec4 lights = texture2D(lightsTex, texCoord);
    //vec4 emission = texture2D(emissionTex, texCoord);
    //vec4 depth = texture2D(depthTex, texCoord);
    float ambient = 0.5;

    //vec4 color = mix(diffuse, lights, 0.9) * (2.0 * dot(lights, normals));
    //vec4 color = ambient * diffuse + diffuse * 2.0 * lights * dot(lights.xyz, normals.xyz);
    //vec4 color =  (diffuse + lights) * dot(lights, normals) * dot(lights.z, normals.z + diffuse.z); // Efecto extraño rollo blur XD
    //vec4 color = depth * ambient * ((diffuse + lights) * dot(lights, normals) * 2.0 * dot(lights.z, normals.z))/*+ (emission * diffuse) * ambient*/;
    vec4 color = ambient * (diffuse + lights) * dot(lights, normals) * 2.0 * dot(lights.z, normals.z);
    gl_FragColor = color;
    //gl_FragColor = depth;
    //gl_FragColor = emission;
    //gl_FragColor = normals;
    //gl_FragColor = lights;
    //gl_FragColor = (diffuse + lights);

}
