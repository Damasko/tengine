#!/bin/python2.7

import sys

def run():

    if (len(sys.argv) != 6):
        return
        
    filename = str(sys.argv[1])
    print(filename)
    tileW = int(sys.argv[2])
    tileH = int(sys.argv[3])

    imageW = int(sys.argv[4])
    imageH = int(sys.argv[5])

    howManyX = imageW / tileW
    howManyY = imageH / tileH

    if ((imageW % tileW) != 0):
        print("tileW is not multiple of imageW")
        return
        
    if ((imageH % tileH) != 0):
        print("tileW is not multiple of imageW")
        return
        
    file = open(filename, 'w')

    count = 1
    for i in range(howManyX):
        for j in range(howManyY):
            file.write(str(count) + ":" + str(j * tileW) + "|" + str(i * tileH) + "|" + str(tileW) + "|" + str(tileH) + "|" + "1:s\n")
            count += 1


    file.close()

     
run()
